//
//  NewsArticleDataProvider.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class NewsArticleDataProvider: NSObject, UITableViewDataSource, UITableViewDelegate, NewsArticleManagerSettable {

    var newsArticleManager: NewsArticleManager?

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let newsArticleManager = newsArticleManager else { return 0 }
        return newsArticleManager.newsArticleCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: newsArticleCellIdentifier, for: indexPath) as? NewsArticleCell {

            if let newsArticleManager = newsArticleManager {
                cell.configCell(with: newsArticleManager.newsArticles[indexPath.row])
            }
            return cell
        } else {
            return NewsArticleCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name(newsArticleSelectedNotification), object: nil, userInfo: [indexInfo: indexPath.row])
    }
}
