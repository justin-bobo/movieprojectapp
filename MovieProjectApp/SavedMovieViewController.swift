//
//  SavedMovieViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/17/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class SavedMovieViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var dataProvider: (UITableViewDataSource & UITableViewDelegate & MovieManagerSettable)?
    var movieManager: MovieManager?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.dataSource = dataProvider
        tableView?.delegate = dataProvider

        movieManager = MovieManager.sharedInstance
        dataProvider?.movieManager = movieManager
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView?.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(showDetails(sender:)), name: NSNotification.Name(savedMovieSelectedNotification), object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(savedMovieSelectedNotification), object: nil)
    }

    func showDetails(sender: NSNotification) {
        guard let index = sender.userInfo?[indexInfo] as? Int else { return }
        guard let movieManager = movieManager else { return }

        if let nextViewController = storyboard?.instantiateViewController(withIdentifier: movieDetailViewControllerIdentifier) as? MovieDetailViewController {
            nextViewController.movieInfo = (movieManager, index)

            guard let nextViewControllerIndex = nextViewController.movieInfo?.1 else { return }

            nextViewController.saveButtonType = UIBarButtonItem(barButtonSystemItem: .trash, target: nextViewController, action: #selector(nextViewController.removeMovie))
            nextViewController.movie = nextViewController.movieInfo?.0.savedMovies[nextViewControllerIndex]

            nextViewController.alert = UIAlertController(title: "Movie Removed", message: "You have removed \(nextViewController.movie?.title ?? "this movie") from your saved movies.", preferredStyle: .alert)
            nextViewController.alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

            navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
}
