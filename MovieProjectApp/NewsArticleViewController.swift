//
//  NewsArticleViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class NewsArticleViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var dataProvider: (UITableViewDataSource & UITableViewDelegate & NewsArticleManagerSettable)?
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView?
    var refreshControl: UIRefreshControl?
    var newsArticleManager: NewsArticleManager?
    var queryParameters = [String: String]()
    var apiClient: APIClient?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.dataSource = dataProvider
        tableView?.delegate = dataProvider

        apiClient = APIClient()

        refreshControl = UIRefreshControl()

        tableView?.refreshControl = refreshControl

        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)

        newsArticleManager = NewsArticleManager.sharedInstance
        dataProvider?.newsArticleManager = newsArticleManager

        NotificationCenter.default.addObserver(self, selector: #selector(fetchAndLoadArticles(sender:)), name: NSNotification.Name(newsArticlesLoadedNotification), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(showDetails(sender:)), name: NSNotification.Name(newsArticleSelectedNotification), object: nil)

        NotificationCenter.default.post(name: NSNotification.Name(newsArticlesLoadedNotification), object: nil, userInfo: nil)

        activityIndicatorView?.startAnimating()
    }

    func showDetails(sender: NSNotification) {
        guard let index = sender.userInfo?[indexInfo] as? Int else { return }
        guard let newsArticleManager = newsArticleManager else { return }

        if let nextViewController = storyboard?.instantiateViewController(withIdentifier: newsArticleDetailViewControllerIdentifier) as? NewsArticleDetailViewController {
            nextViewController.newsArticleInfo = (newsArticleManager, index)
            navigationController?.pushViewController(nextViewController, animated: true)
        }
    }

    func fetchAndLoadArticles(sender: NSNotification) {

        queryParameters[newsArticleSourceParameterKey] = "entertainment-weekly"
        queryParameters[newsArticleAPIParameterKey] = "a7d7b2c88f77419081cabf6c5e612784"

        apiClient?.getNewsArticleRequest(scheme: httpsScheme, host: newsArticleAPIHost, path: newsArticleAPIPath, queryItems: queryParameters) { (newsArticles, error) in
            if let loadedNewsArticles = newsArticles {
                self.newsArticleManager?.newsArticles = loadedNewsArticles
                self.tableView?.reloadData()
                self.activityIndicatorView?.stopAnimating()
            } else {
                // handle error
            }
        }
    }

    func refreshData() {
        NotificationCenter.default.post(name: NSNotification.Name(newsArticlesLoadedNotification), object: nil, userInfo: nil)
        refreshControl?.endRefreshing()
    }
}

@objc protocol NewsArticleManagerSettable {
    var newsArticleManager: NewsArticleManager? { get set }
}
