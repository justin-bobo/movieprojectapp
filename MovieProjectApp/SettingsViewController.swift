//
//  SettingsViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/12/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class SettingsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func logoutTapped(_ sender: UIButton) {
        try? FIRAuth.auth()?.signOut()
        GIDSignIn.sharedInstance().signOut()
        
        FIRAuth.auth()?.addStateDidChangeListener { (auth, user) in
            if user == nil {
                self.performSegue(withIdentifier: "logOutSegue", sender: self)
            }
        }
    }
}
