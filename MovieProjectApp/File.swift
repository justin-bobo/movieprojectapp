//
//  File.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation

class File: Equatable {
    internal let id: String
    internal let title: String
    internal let data: Data
    internal let url: URL?

    init(id: String, title: String, data: Data, url: URL? = nil) {
        self.id = id
        self.title = title
        self.data = data
        self.url = url
    }
}

func ==(lhs: File, rhs: File) -> Bool {
    if (lhs.id != rhs.id || lhs.title != rhs.title || lhs.data != rhs.data || lhs.url != rhs.url) {
        return false
    }
    return true
}
