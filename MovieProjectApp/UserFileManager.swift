//
//  UserFileManager.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation

class UserFileManager: NSObject {

    static let sharedInstance = UserFileManager()

    var savedFilesCount: Int {
        return savedFiles.count
    }

    var savedFiles = [File]()

    func add(file: File) {
        if !savedFiles.contains(file) {
            savedFiles.append(file)
        }
    }

    func remove(at index: Int) {
        savedFiles.remove(at: index)
    }

    func returnFile(at index: Int) -> File {
        return savedFiles[index]
    }
}
