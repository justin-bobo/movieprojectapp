//
//  MovieResultsDataProvider.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/27/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class MovieResultsDataProvider: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MovieManagerSettable {

    var movieManager: MovieManager?

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let movieManager = movieManager else { return 0 }
        guard let movieResultsCount = movieManager.movieResultsCount else { return 0 }
        return movieResultsCount
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: movieResultCellIdentifier, for: indexPath) as? MovieResultCell {

            if let movieManager = movieManager, let movieResults = movieManager.movieResults {
                cell.configCell(with: movieResults[indexPath.row])
            }
            return cell
        } else {
            return MovieResultCell()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name(movieResultSelectedNotification), object: nil, userInfo: [indexInfo: indexPath.item])
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
