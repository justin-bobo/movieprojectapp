//
//  FileCell.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class FileCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel?

    func configCell(with file: File) {
        titleLabel?.text = file.title
    }
}
