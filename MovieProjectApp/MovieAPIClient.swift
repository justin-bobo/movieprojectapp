//
//  MovieAPIClient.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/24/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension APIClient {

    func getMovieRequest(scheme: String, host: String, path: String, queryItems: [String: String], completion: @escaping (_ movieResults: [Movie]?, _ error: Error?) -> Void) {

        guard let url = createURLWithComponents(scheme: scheme, host: host, path: path, queryItems: queryItems) else { return }

        Alamofire.request(url).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)

                var moviesArray = [Movie]()
                if let movies = json.arrayObject as? [[String: Any]] {

                    for movie in movies {
                        if let id = movie[showIdAttribute] as? Int, let title = movie[titleAttribute] as? String, let releaseYear = movie[releaseYearAttribute] as? String, let rating = movie[ratingAttribute] as? String, let category = movie[categoryAttribute] as? String, let cast = movie[castAttribute] as? String, let director = movie[directorAttribute] as? String, let summary = movie[summaryAttribute] as? String, let runtime = movie[runtimeAttribute] as? String, let poster = movie[posterAttribute] as? String {

                            let newMovie = Movie(id: id, title: title, releaseYear: releaseYear, rating: rating, category: category, cast: cast, director: director, summary: summary, runtime: runtime, poster: poster)

                            moviesArray.append(newMovie)
                        }
                    }
                }
                completion(moviesArray, nil)

            case .failure(let error):
                completion(nil, error)
                print(error)
            }
        }
    }
}
