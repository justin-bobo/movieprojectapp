//
//  NewsArticleAPIClient.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/24/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension APIClient {

    func getNewsArticleRequest(scheme: String, host: String, path: String, queryItems: [String: String], completion: @escaping (_ newsArticles: [NewsArticle]?, _ error: Error?) -> Void) {

        guard let url = createURLWithComponents(scheme: scheme, host: host, path: path, queryItems: queryItems) else { return }

        Alamofire.request(url).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)

                var articlesArray = [NewsArticle]()
                if let articles = json[articlesJSON].arrayObject as? [[String: String]] {

                    for article in articles {
                        if let title = article[articleTitleAttribute], let description = article[descriptionAttribute], let url = article[urlAttribute], let imageURL = article[imageURLAttribute] {
                            let newsArticle = NewsArticle(title: title, description: description, url: url, imageUrl: imageURL)
                            articlesArray.append(newsArticle)
                        }
                    }
                }
                completion(articlesArray, nil)

            case .failure(let error):
                completion(nil, error)
                print(error)
            }
        }
    }
}
