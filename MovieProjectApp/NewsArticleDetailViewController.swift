//
//  NewsArticleDetailViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/21/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import WebKit

class NewsArticleDetailViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {

    var articleTitle: String?
    var articleDescription: String?
    var url: URL?
    var imageURL: String?
    var webView: WKWebView?
    var activityIndicatorView: UIActivityIndicatorView?
    var newsArticleInfo: (NewsArticleManager, Int)?

    override func loadView() {
        webView = WKWebView()
        webView?.navigationDelegate = self
        webView?.uiDelegate = self
        view = webView
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let newsArticleInfo = newsArticleInfo else { return }
        let newsArticle = newsArticleInfo.0.returnNewsArticle(at: newsArticleInfo.1)
        articleTitle = newsArticle.title
        articleDescription = newsArticle.description
        imageURL = newsArticle.imageUrl

        url = URL(string: newsArticle.url)
        guard let url = url else { return }

        _ = webView?.load(URLRequest(url: url))
        webView?.allowsBackForwardNavigationGestures = true
    }

    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView?.center = self.view.center
        activityIndicatorView?.hidesWhenStopped = true
        activityIndicatorView?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        guard let activityIndicatorView = activityIndicatorView else { return }
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicatorView?.stopAnimating()
    }

    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        activityIndicatorView?.stopAnimating()
    }
}
