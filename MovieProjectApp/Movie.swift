//
//  Movie.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/15/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation

class Movie: Equatable {
    internal let id: Int
    internal let title: String
    internal let releaseYear: String
    internal let rating: String
    internal let category: String
    internal let cast: String
    internal let director: String
    internal let summary: String
    internal let runtime: String
    internal let poster: String

    init(id: Int, title: String, releaseYear: String, rating: String, category: String, cast: String, director: String, summary: String, runtime: String, poster: String) {
        self.id = id
        self.title = title
        self.releaseYear = releaseYear
        self.rating = rating
        self.category = category
        self.cast = cast
        self.director = director
        self.summary = summary
        self.runtime = runtime
        self.poster = poster
    }
}

func ==(lhs: Movie, rhs: Movie) -> Bool {
    if (lhs.id != rhs.id || lhs.title != rhs.title || lhs.releaseYear != rhs.releaseYear || lhs.rating != rhs.rating || lhs.category != rhs.category || lhs.cast != rhs.cast || lhs.director != rhs.director || lhs.summary != rhs.summary || lhs.runtime != rhs.runtime || lhs.poster != rhs.poster) {
        return false
    }
    return true
}
