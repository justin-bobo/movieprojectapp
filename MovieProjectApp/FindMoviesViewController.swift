//
//  FindMoviesViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/25/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class FindMoviesViewController: UIViewController {

    @IBOutlet weak var actorTextField: UITextField?
    @IBOutlet weak var directorTextField: UITextField?
    @IBOutlet weak var searchButton: UIButton?
    @IBOutlet weak var dataProvider: (UITextFieldDelegate & MovieManagerSettable)?
    var needParametersAlert: UIAlertController?
    var queryParameters = [String: String]()
    var apiClient: APIClient?

    var movieManager: MovieManager?

    override func viewDidLoad() {
        super.viewDidLoad()

        actorTextField?.delegate = dataProvider
        directorTextField?.delegate = dataProvider

        movieManager = MovieManager.sharedInstance
        movieManager?.movieResults = nil
        dataProvider?.movieManager = movieManager

        apiClient = APIClient()

        NotificationCenter.default.addObserver(self, selector: #selector(showResults(sender:)), name: NSNotification.Name(movieSearchNotification), object: nil)
    }

    func showResults(sender: NSNotification) {

        if let nextViewController = storyboard?.instantiateViewController(withIdentifier: movieResultsViewControllerIdentifier) as? MovieResultsViewController {

            if actorTextField?.text == "" && directorTextField?.text == "" {
                needParametersAlert = UIAlertController(title: "Need Search Criteria", message: "Please enter search criteria.", preferredStyle: .alert)
                needParametersAlert?.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(needParametersAlert!, animated: true, completion: nil)
                return
            }

            if let actorText = actorTextField?.text, (actorTextField?.text?.characters.count)! > 0 {
                queryParameters[movieActorParameterKey] = actorText
            }

            if let directorText = directorTextField?.text, (directorTextField?.text?.characters.count)! > 0 {
                queryParameters[movieDirectorParameterKey] = directorText
            }

            apiClient?.getMovieRequest(scheme: httpsScheme, host: movieAPIHost, path: movieAPIPath, queryItems: queryParameters) { (movie, error) in

                guard let movie = movie else {
                    // handle error
                    return
                }

                if movie.count > 0 {
                    nextViewController.movieManager = self.movieManager
                    nextViewController.movieManager?.movieResults = movie
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                } else {
                    // present an alert that tells user no results
                }
            }
        }
    }
    @IBAction func search(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(movieSearchNotification), object: nil, userInfo: nil)
    }
}
