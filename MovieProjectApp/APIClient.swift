//
//  APIClient.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/23/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIClient {

    func createURLWithComponents(scheme: String, host: String, path: String, queryItems: [String: String]) -> URL? {

        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.queryItems = []

        for (key, value) in queryItems {
            let queryItem = URLQueryItem(name: key, value: value)
            urlComponents.queryItems?.append(queryItem)
        }

        guard let url = urlComponents.url else { return nil }
        return url
    }
}
