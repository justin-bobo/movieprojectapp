//
//  NewsArticleManager.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation

class NewsArticleManager: NSObject {

    static let sharedInstance = NewsArticleManager()

    var newsArticleCount: Int {
        return newsArticles.count
    }
    var newsArticles = [NewsArticle]()

    func add(newsArticle: NewsArticle) {
        if !newsArticles.contains(newsArticle) {
            newsArticles.append(newsArticle)
        }
    }

    func remove(at index: Int) {
        newsArticles.remove(at: index)
    }

    func returnNewsArticle(at index: Int) -> NewsArticle {
        return newsArticles[index]
    }
}
