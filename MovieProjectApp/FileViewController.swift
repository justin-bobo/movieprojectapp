//
//  FileViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import MobileCoreServices
import PDFReader

class FileViewController: UIViewController, UIDocumentMenuDelegate, UIDocumentPickerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tableView: UITableView?
    @IBOutlet var dataProvider: (UITableViewDelegate & UITableViewDataSource & UserFileManagerSettable)?
    var userFileManager: UserFileManager?
    var importMenu: UIDocumentMenuViewController?
    var imagePicker: UIImagePickerController?
    var imageData: Data?
    var file: File?
    var fileName: String?
    var fileURL: URL?
    var nextViewControllerIdentifier: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.dataSource = dataProvider
        tableView?.delegate = dataProvider

        userFileManager = UserFileManager.sharedInstance
        dataProvider?.userFileManager = userFileManager

        NotificationCenter.default.addObserver(self, selector: #selector(showDetails(sender:)), name: NSNotification.Name(fileSelectedNotification), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView?.reloadData()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // if a selected video file is corrupted, app freezes - need to call apple to figure out the issue
        if let pickedVideoURL = info[UIImagePickerControllerMediaURL] as? URL {
            guard let fileData = try? Data(contentsOf: pickedVideoURL) else { return }
            fileName = pickedVideoURL.lastPathComponent
            guard let fileName = fileName else { return }
            let uuid = UUID().uuidString
            file = File(id: uuid, title: fileName, data: fileData, url: pickedVideoURL)
            guard let file = file else { return }
            userFileManager?.add(file: file)
            tableView?.reloadData()
            dismiss(animated: true, completion: nil)
        } else if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageData = UIImageJPEGRepresentation(pickedImage, 1.0)
            fileName = (info[UIImagePickerControllerReferenceURL] as? URL)?.lastPathComponent ?? "Image.JPG"
            let uuid = UUID().uuidString

            guard let fileName = fileName else { return }
            guard let imageData = imageData else { return }

            file = File(id: uuid, title: fileName, data: imageData)
            guard let file = file else { return }
            userFileManager?.add(file: file)
            tableView?.reloadData()
            dismiss(animated: true, completion: nil)
        } else {
            // present alertview with error
            dismiss(animated: true, completion: nil)
        }
    }

    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        fileURL = url
        print(url)
        guard let fileData = try? Data(contentsOf: url) else { return }
        fileName = url.lastPathComponent
        guard let fileName = fileName else { return }
        let uuid = UUID().uuidString
        file = File(id: uuid, title: fileName, data: fileData, url: url)
        guard let file = file else { return }
        userFileManager?.add(file: file)
        tableView?.reloadData()
    }

    func showDetails(sender: NSNotification) {
        guard let index = sender.userInfo?[indexInfo] as? Int else { return }
        guard let userFileManager = userFileManager else { return }

        guard let fileName = fileName else { return }

        if fileName.hasSuffix(".jpeg") || fileName.hasSuffix(".jpg") || fileName.hasSuffix(".png") || fileName.hasSuffix(".JPEG") || fileName.hasSuffix(".JPG") || fileName.hasSuffix(".PNG") {
            nextViewControllerIdentifier = fileImageDetailViewControllerIdentifier
        } else if fileName.hasSuffix(".mov") || fileName.hasSuffix(".mp4") || fileName.hasSuffix(".MOV") || fileName.hasSuffix(".MP4") {
            nextViewControllerIdentifier = fileMovieDetailViewControllerIdentifier
        } else if fileName.hasSuffix(".pdf") || fileName.hasSuffix(".PDF") {
            nextViewControllerIdentifier = filePDFDetailViewControllerIdentifier
        } else {
            nextViewControllerIdentifier = fileDefaultDetailViewControllerIdentifier
        }

        guard let nextViewControllerIdentifier = nextViewControllerIdentifier else { return }

        chooseNextViewController(viewControllerIdentifier: nextViewControllerIdentifier, userFileManager: userFileManager, index: index)
    }

    func chooseNextViewController(viewControllerIdentifier: String, userFileManager: UserFileManager, index: Int) {
        guard let nextViewControllerIdentifier = nextViewControllerIdentifier else { return }

        switch nextViewControllerIdentifier {
        case fileImageDetailViewControllerIdentifier:
            if let nextViewController = storyboard?.instantiateViewController(withIdentifier: nextViewControllerIdentifier) as? FileImageDetailViewController {
                nextViewController.fileInfo = (userFileManager, index)

                navigationController?.pushViewController(nextViewController, animated: true)
            }
        case fileMovieDetailViewControllerIdentifier:
            if let nextViewController = storyboard?.instantiateViewController(withIdentifier: nextViewControllerIdentifier) as? FileMovieDetailViewController {
                nextViewController.fileInfo = (userFileManager, index)

                navigationController?.pushViewController(nextViewController, animated: true)
            }
        case filePDFDetailViewControllerIdentifier:
            guard let fileURL = fileURL else { return }
            let document = PDFDocument(url: fileURL)
            let readerController = PDFViewController.createNew(with: document!)
            navigationController?.pushViewController(readerController, animated: true)
        default:
            if let nextViewController = storyboard?.instantiateViewController(withIdentifier: nextViewControllerIdentifier) as? FileDefaultDetailViewController {
                nextViewController.fileInfo = (userFileManager, index)
                navigationController?.pushViewController(nextViewController, animated: true)
            }
        }
    }

    @IBAction func addFileTapped(_ sender: UIBarButtonItem) {
        importMenu = UIDocumentMenuViewController(documentTypes: [kUTTypeSpreadsheet as String, kUTTypeData as String, kUTTypeCompositeContent as String, kUTTypeImage as String, kUTTypeAudiovisualContent as String], in: .import)
        guard let importMenu = importMenu else { return }
        importMenu.delegate = self
        
        imagePicker = UIImagePickerController()
        guard let imagePicker = imagePicker else { return }
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        
        importMenu.addOption(withTitle: "Choose from Saved Photos and Videos", image: UIImage(named: "ic_folder_white_36pt"), order: .first) {
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        importMenu.addOption(withTitle: "Capture Photo or Video", image: UIImage(named: "ic_photo_camera_white_36pt"), order: .first) {
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        present(importMenu, animated: true, completion: nil)
    }

    @IBAction func settingsButtonTapped(_ sender: UIBarButtonItem) {
        guard let settingsViewController = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: settingsViewControllerIdentifier) as? SettingsViewController else { return }
        self.present(settingsViewController, animated: true, completion: nil)
    }
}
