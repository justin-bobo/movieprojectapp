//
//  NewsArticle.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/15/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation

class NewsArticle: Equatable {
    internal let title: String
    internal let description: String
    internal let url: String
    internal let imageUrl: String

    init(title: String, description: String, url: String, imageUrl: String) {
        self.title = title
        self.description = description
        self.url = url
        self.imageUrl = imageUrl
    }
}

func ==(lhs: NewsArticle, rhs: NewsArticle) -> Bool {
    if lhs.title != rhs.title || lhs.description != rhs.description || lhs.url != rhs.url || lhs.imageUrl != rhs.imageUrl {
        return false
    }
    return true
}
