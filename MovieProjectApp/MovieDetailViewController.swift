//
//  MovieDetailViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/19/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import SDWebImage

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var releaseYearLabel: UILabel?
    @IBOutlet weak var ratingLabel: UILabel?
    @IBOutlet weak var categoryLabel: UILabel?
    @IBOutlet weak var castLabel: UILabel?
    @IBOutlet weak var directorLabel: UILabel?
    @IBOutlet weak var summaryLabel: UILabel?
    @IBOutlet weak var runtimeLabel: UILabel?
    @IBOutlet weak var posterImage: UIImageView?
    var alert: UIAlertController?
    var alertAction: UIAlertAction?
    var saveButtonType: UIBarButtonItem?
    var movie: Movie?

    var movieInfo: (MovieManager, Int)?

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let saveButtonType = saveButtonType else { return }
        self.navigationItem.rightBarButtonItem = saveButtonType
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let movie = movie else { return }
        setMovieProperties(movie: movie)

        guard let movieInfo = movieInfo else { return }

        if movieInfo.0.savedMovies.contains(movie) {
            saveButtonType = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(removeMovie))
            self.navigationItem.rightBarButtonItem = saveButtonType

            alert = UIAlertController(title: "Movie Removed", message: "You have removed \(movie.title) from your saved movies.", preferredStyle: .alert)
        } else {
            saveButtonType = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveMovie))
            self.navigationItem.rightBarButtonItem = saveButtonType

            alert = UIAlertController(title: "Movie Saved", message: "You have saved \(movie.title).", preferredStyle: .alert)
        }

        guard let alert = alert else { return }
        guard let alertAction = alertAction else { return }
        alert.addAction(alertAction)
    }

    func setMovieProperties(movie: Movie) {
        titleLabel?.text = movie.title
        titleLabel?.sizeToFit()
        releaseYearLabel?.text = String(movie.releaseYear)
        releaseYearLabel?.sizeToFit()
        ratingLabel?.text = String(movie.rating)
        ratingLabel?.sizeToFit()
        categoryLabel?.text = movie.category
        categoryLabel?.sizeToFit()
        castLabel?.text = movie.cast
        castLabel?.sizeToFit()
        directorLabel?.text = movie.director
        directorLabel?.sizeToFit()
        summaryLabel?.text = movie.summary
        summaryLabel?.sizeToFit()
        runtimeLabel?.text = movie.runtime
        runtimeLabel?.sizeToFit()

        guard let imageURL = URL(string: movie.poster) else { return }
        posterImage?.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "image-unavailable-placeholder"))
        posterImage?.dropShadow()
    }

    func saveMovie() {
        guard let movie = movie else { return }
        movieInfo?.0.saveMovie(movie: movie)

        guard let currentAlert = alert else { return }

        self.present(currentAlert, animated: true, completion: nil)

        saveButtonType = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(removeMovie))
        self.navigationItem.rightBarButtonItem = saveButtonType

        alert = UIAlertController(title: "Movie Removed", message: "You have removed \(movie.title) from your saved movies.", preferredStyle: .alert)
        guard let alertAction = alertAction else { return }
        alert?.addAction(alertAction)
    }

    func removeMovie() {
        guard let movie = movie else { return }
        let newSavedMovies = movieInfo?.0.savedMovies.filter { $0 != movie }
        movieInfo?.0.savedMovies = newSavedMovies!

        guard let currentAlert = alert else { return }

        self.present(currentAlert, animated: true, completion: nil)

        saveButtonType = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveMovie))
        self.navigationItem.rightBarButtonItem = saveButtonType

        alert = UIAlertController(title: "Movie Saved", message: "You have saved \(movie.title).", preferredStyle: .alert)
        guard let alertAction = alertAction else { return }
        alert?.addAction(alertAction)
    }
}
