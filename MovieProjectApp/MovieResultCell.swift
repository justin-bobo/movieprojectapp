//
//  MovieResultCell.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/27/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import SDWebImage

class MovieResultCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var releaseYearLabel: UILabel?
    @IBOutlet weak var ratingLabel: UILabel?
    @IBOutlet weak var categoryLabel: UILabel?
    @IBOutlet weak var posterImage: UIImageView?

    func configCell(with movieResult: Movie) {
        titleLabel?.text = movieResult.title
        releaseYearLabel?.text = movieResult.releaseYear
        ratingLabel?.text = movieResult.rating
        categoryLabel?.text = movieResult.category

        guard let posterImageURL = URL(string: movieResult.poster) else { return }
        posterImage?.sd_setImage(with: posterImageURL, placeholderImage: UIImage(named: "image-unavailable-placeholder"))
    }
}
