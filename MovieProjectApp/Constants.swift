//
//  Constants.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/25/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation

// MARK: Notifications
let movieResultSelectedNotification = "MovieResultSelectedNotification"
let savedMovieSelectedNotification = "SavedMovieSelectedNotification"
let newsArticleSelectedNotification = "NewsArticleSelectedNotification"
let movieSearchNotification = "MovieSearchNotification"
let newsArticlesLoadedNotification = "NewsArticlesLoadedNotification"
let fileSelectedNotification = "FileSelectedNotification"

// MARK: Storyboard
let mainStoryboard = "Main"

// MARK: MovieAPIClient
let showIdAttribute = "show_id"
let titleAttribute = "show_title"
let releaseYearAttribute = "release_year"
let ratingAttribute = "rating"
let categoryAttribute = "category"
let castAttribute = "show_cast"
let directorAttribute = "director"
let summaryAttribute = "summary"
let runtimeAttribute = "runtime"
let posterAttribute = "poster"

// MARK: NewsArticleAPIClient
let articlesJSON = "articles"
let articleTitleAttribute = "title"
let descriptionAttribute = "description"
let urlAttribute = "url"
let imageURLAttribute = "urlToImage"

// MARK: URLCreation
let httpsScheme = "https"
let movieAPIHost = "netflixroulette.net"
let movieAPIPath = "/api/api.php"
let movieTitleParameterKey = "title"
let movieYearParameterKey = "year"
let movieActorParameterKey = "actor"
let movieDirectorParameterKey = "director"

let newsArticleAPIHost = "newsapi.org"
let newsArticleAPIPath = "/v1/articles"
let newsArticleSourceParameterKey = "source"
let newsArticleAPIParameterKey = "apiKey"

// MARK: Notifications
let indexInfo = "index"

// MARK: ViewController Identifiers
let newsArticleViewControllerIdentifier = "NewsArticleViewController"
let newsArticleDetailViewControllerIdentifier = "NewsArticleDetailViewController"
let savedMovieViewControllerIdentifier = "SavedMovieViewController"
let findMoviesViewControllerIdentifier = "FindMoviesViewController"
let movieDetailViewControllerIdentifier = "MovieDetailViewController"
let movieResultsViewControllerIdentifier = "MovieResultsViewController"
let fileViewControllerIdentifier = "FileViewController"
let fileDefaultDetailViewControllerIdentifier = "FileDefaultDetailViewController"
let fileMovieDetailViewControllerIdentifier = "FileMovieDetailViewController"
let fileImageDetailViewControllerIdentifier = "FileImageDetailViewController"
let filePDFDetailViewControllerIdentifier = "FilePDFDetailViewController"
let settingsViewControllerIdentifier = "SettingsViewController"

// MARK: Cell Identifiers
let newsArticleCellIdentifier = "NewsArticleCell"
let savedMovieCellIdentifier = "SavedMovieCell"
let movieResultCellIdentifier = "MovieResultCell"
let fileCellIdentifier = "FileCell"
