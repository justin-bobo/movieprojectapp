//
//  SavedMovieDataProvider.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/17/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class SavedMovieDataProvider: NSObject, UITableViewDataSource, UITableViewDelegate, MovieManagerSettable {

    var movieManager: MovieManager?

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let movieManager = movieManager else { return 0 }
        return movieManager.savedMovieCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: savedMovieCellIdentifier, for: indexPath) as? SavedMovieCell {

            if let movieManager = movieManager {
                cell.configCell(with: movieManager.savedMovies[indexPath.row])
            }
            return cell
        } else {
            return SavedMovieCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name(savedMovieSelectedNotification), object: nil, userInfo: [indexInfo: indexPath.row])
    }
}

@objc protocol MovieManagerSettable {
    var movieManager: MovieManager? { get set }
}
