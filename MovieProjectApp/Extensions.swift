//
//  Extensions.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/2/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

extension UIView {

    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1

        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
}
