//
//  MovieManager.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation

class MovieManager: NSObject {

    static let sharedInstance = MovieManager()

    var savedMovieCount: Int {
        return savedMovies.count
    }
    var savedMovies = [Movie]()

    var movieResultsCount: Int? {
        guard let movieResults = movieResults else { return 0 }
        return movieResults.count
    }
    var movieResults: [Movie]?

    func saveMovie(movie: Movie) {
        if !savedMovies.contains(movie) {
            savedMovies.append(movie)
        }
    }

    func removeSavedMovie(at index: Int) {
        savedMovies.remove(at: index)
    }

    func returnSavedMovie(at index: Int) -> Movie {
        return savedMovies[index]
    }

    func returnMovieResult(at index: Int) -> Movie? {
        return movieResults?[index]
    }
}

