//
//  FileMovieDetailViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/9/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class FileMovieDetailViewController: AVPlayerViewController {

    var fileInfo: (UserFileManager, Int)?
    var url: URL?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let fileInfo = fileInfo else { return }
        let file = fileInfo.0.returnFile(at: fileInfo.1)
        
        url = file.url
        guard let url = url else { return }
        
        self.player = AVPlayer(url: url)
    }
}
