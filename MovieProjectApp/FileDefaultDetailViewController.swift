//
//  FileDefaultDetailViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import WebKit

class FileDefaultDetailViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {

    var fileTitle: String?
    var url: URL?
    var imageURL: String?
    var webView: WKWebView?
    var fileInfo: (UserFileManager, Int)?

    override func loadView() {
        webView = WKWebView()
        webView?.navigationDelegate = self
        webView?.uiDelegate = self
        view = webView
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let fileInfo = fileInfo else { return }
        let file = fileInfo.0.returnFile(at: fileInfo.1)

        url = file.url
        guard let url = url else { return }

        _ = webView?.load(URLRequest(url: url))
        webView?.allowsBackForwardNavigationGestures = true

        self.title = file.title
    }
}
