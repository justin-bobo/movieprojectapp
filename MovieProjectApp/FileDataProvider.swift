//
//  FileDataProvider.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import Foundation

class FileDataProvider: NSObject, UITableViewDelegate, UITableViewDataSource, UserFileManagerSettable {
    
    var userFileManager: UserFileManager?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let userFileManager = userFileManager else { return 0 }
        return userFileManager.savedFilesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: fileCellIdentifier, for: indexPath) as? FileCell {
            
            if let userFileManager = userFileManager {
                cell.configCell(with: userFileManager.savedFiles[indexPath.row])
            }
            return cell
        } else {
            return FileCell()
        }
    }
    // consider adding file type to userInfo - depending on file type, you can determine whether to transition to fileDetailVC or the PDFVC
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name(fileSelectedNotification), object: nil, userInfo: [indexInfo: indexPath.row])
    }
}

@objc protocol UserFileManagerSettable {
    var userFileManager: UserFileManager? { get set }
}
