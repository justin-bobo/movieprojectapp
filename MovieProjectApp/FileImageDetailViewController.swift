//
//  FileImageDetailViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/9/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class FileImageDetailViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var imageView: UIImageView?
    var fileInfo: (UserFileManager, Int)?
    var imageData: Data?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView?.minimumZoomScale = 1.0
        self.scrollView?.maximumZoomScale = 6.0
        
        guard let fileInfo = fileInfo else { return }
        let file = fileInfo.0.returnFile(at: fileInfo.1)
        
        imageData = file.data
        guard let imageData = imageData else { return }
        imageView?.image = UIImage(data: imageData)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
