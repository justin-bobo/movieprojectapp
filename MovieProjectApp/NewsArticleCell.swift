//
//  NewsArticleCell.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import SDWebImage

class NewsArticleCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var imageLabel: UIImageView?

    func configCell(with newsArticle: NewsArticle) {
        titleLabel?.text = newsArticle.title
        descriptionLabel?.text = newsArticle.description

        guard let imageURL = URL(string: newsArticle.imageUrl) else { return }
        imageLabel?.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "image-unavailable-placeholder"))
    }
}
