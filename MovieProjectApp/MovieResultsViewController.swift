//
//  MovieResultsViewController.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/26/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout

class MovieResultsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet var dataProvider: (UICollectionViewDataSource & UICollectionViewDelegate & MovieManagerSettable)?

    var movieManager: MovieManager?

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.dataSource = dataProvider
        collectionView?.delegate = dataProvider

        dataProvider?.movieManager = movieManager

        let layout = AnimatedCollectionViewLayout()
        layout.animator = LinearCardAttributesAnimator()
        layout.scrollDirection = .horizontal
        collectionView?.collectionViewLayout = layout
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(showDetails(sender:)), name: NSNotification.Name(movieResultSelectedNotification), object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(movieResultSelectedNotification), object: nil)
    }

    func showDetails(sender: NSNotification) {
        guard let index = sender.userInfo?[indexInfo] as? Int else { return }
        guard let movieManager = movieManager else { return }

        if let nextViewController = storyboard?.instantiateViewController(withIdentifier: movieDetailViewControllerIdentifier) as? MovieDetailViewController {
            nextViewController.movieInfo = (movieManager, index)

            guard let nextViewControllerIndex = nextViewController.movieInfo?.1 else { return }

            nextViewController.saveButtonType = UIBarButtonItem(barButtonSystemItem: .save, target: nextViewController, action: #selector(nextViewController.saveMovie))
            nextViewController.movie = nextViewController.movieInfo?.0.movieResults?[nextViewControllerIndex]

            nextViewController.alert = UIAlertController(title: "Movie Saved", message: "You have saved \(nextViewController.movie?.title ?? "this movie").", preferredStyle: .alert)
            nextViewController.alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

            navigationController?.pushViewController(nextViewController, animated: true)
        }
    }

    deinit {
        movieManager?.movieResults = nil
    }
}
