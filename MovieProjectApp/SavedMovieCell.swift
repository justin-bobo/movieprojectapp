//
//  SavedMovieCell.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/17/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import UIKit

class SavedMovieCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var releaseYearLabel: UILabel?
    @IBOutlet weak var ratingLabel: UILabel?
    @IBOutlet weak var categoryLabel: UILabel?
    @IBOutlet weak var runtimeLabel: UILabel?
    @IBOutlet weak var posterImage: UIImageView?

    func configCell(with savedMovie: Movie) {
        titleLabel?.text = savedMovie.title
        releaseYearLabel?.text = String(savedMovie.releaseYear)
        ratingLabel?.text = String(savedMovie.rating)
        categoryLabel?.text = savedMovie.category
        runtimeLabel?.text = savedMovie.runtime

        guard let posterImageURL = URL(string: savedMovie.poster) else { return }
        posterImage?.sd_setImage(with: posterImageURL, placeholderImage: UIImage(named: "image-unavailable-placeholder"))
    }
}
