//
//  NewsArticleCellTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/17/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class NewsArticleCellTests: XCTestCase {

    var controller: NewsArticleViewController?
    var tableView: UITableView?
    var cell: NewsArticleCell?

    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: newsArticleViewControllerIdentifier) as? NewsArticleViewController
        _ = controller?.view
        tableView = controller?.tableView
        let dataSource = FakeDataSource()
        tableView?.dataSource = dataSource
        tableView?.delegate = dataSource
        cell = tableView?.dequeueReusableCell(withIdentifier: newsArticleCellIdentifier, for: IndexPath(row: 0, section: 0)) as? NewsArticleCell
    }

    override func tearDown() {
        controller = nil
        tableView = nil
        cell = nil
        super.tearDown()
    }

    func test_HasLabelsAndImage() {
        XCTAssertNotNil(cell?.titleLabel)
        XCTAssertNotNil(cell?.descriptionLabel)
        XCTAssertNotNil(cell?.imageLabel)
    }

    func test_ConfigCell_SetsLabelsAndImage() {
        let newsArticle = NewsArticle(title: "Trump", description: "Apple website", url: "", imageUrl: "https://images-na.ssl-images-amazon.com/images/I/21upJE9hHjL._AC_UL320_SR278,320_.jpg")
        cell?.configCell(with: newsArticle)
        XCTAssertEqual(cell?.titleLabel!.text, newsArticle.title)
        XCTAssertEqual(cell?.descriptionLabel!.text, newsArticle.description)
        XCTAssertNotNil(cell?.imageLabel?.image)
    }
}

extension NewsArticleCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return UITableViewCell()
        }
    }
}
