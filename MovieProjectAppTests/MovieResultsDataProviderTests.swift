//
//  MovieResultsDataProviderTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/27/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class MovieResultsDataProviderTests: XCTestCase {

    var sut: MovieResultsDataProvider?
    var controller: MovieResultsViewController?
    var collectionView: UICollectionView?

    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: movieResultsViewControllerIdentifier) as? MovieResultsViewController
        sut = controller?.dataProvider as? MovieResultsDataProvider
        _ = controller?.view
        controller?.movieManager = MovieManager.sharedInstance
        sut?.movieManager = controller?.movieManager
        sut?.movieManager?.movieResults = [Movie]()
        collectionView = controller?.collectionView
        collectionView?.dataSource = sut
        collectionView?.delegate = sut
    }

    override func tearDown() {
        sut = nil
        controller = nil
        collectionView = nil
        super.tearDown()
    }

    func test_NumberOfSectionsEqualsOne() {
        let numberOfSections = controller?.collectionView?.numberOfSections
        XCTAssertEqual(numberOfSections, 1)
    }

    func test_NumberOfRowsEqualsOneWhenMovieManagerIsNil() {
        sut?.movieManager = nil
        XCTAssertEqual(collectionView?.numberOfItems(inSection: 0), 0)
    }
    
    func test_NumberOfRowsEqualsOneWhenMovieResultsIsNil() {
        sut?.movieManager?.movieResults = nil
        XCTAssertEqual(collectionView?.numberOfItems(inSection: 0), 0)
    }
    
    func test_SizeForItemAt_ReturnsCollectionViewBoundsSize() {
        let movie1 = Movie(id: 1, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieManager?.movieResults?.append(movie1) ?? (sut?.movieManager?.movieResults? = [movie1]))
        collectionViewReloadDataAndLayoutIfNeeded(collectionView: collectionView!)
        _ = collectionView?.cellForItem(at: IndexPath(item: 0, section: 0))
        XCTAssertTrue(collectionView?.collectionViewLayout.collectionViewContentSize == collectionView?.bounds.size)
    }

    func test_NumberOfItemsEqualsMovieResultsCount() {
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 1, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieManager?.movieResults?.append(movie1) ?? (sut?.movieManager?.movieResults? = [movie1]))
        XCTAssertEqual(collectionView?.numberOfItems(inSection: 0), 1)
        (sut?.movieManager?.movieResults?.append(movie2) ?? (sut?.movieManager?.movieResults? = [movie2]))
        collectionView?.reloadData()
        XCTAssertEqual(collectionView?.numberOfItems(inSection: 0), 2)
    }
    // looks like there might be a bug testing cellforitem
    func test_CellForItem_ReturnsCustomMovieResultCell() {
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieManager?.movieResults?.append(movie1) ?? (sut?.movieManager?.movieResults? = [movie1]))
        collectionViewReloadDataAndLayoutIfNeeded(collectionView: collectionView!)
        let cell = collectionView?.cellForItem(at: IndexPath(item: 0, section: 0))
        XCTAssertTrue(cell is MovieResultCell)
    }
    // want to know that the cellForRow function is calling dequeueReusableCell
    func test_CellForItem_DequeuesCellFromCollectionView() {
        let mockCollectionView = MockCollectionView.mockCollectionView(with: sut!)
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieManager?.movieResults?.append(movie1) ?? (sut?.movieManager?.movieResults? = [movie1]))
        collectionViewReloadDataAndLayoutIfNeeded(collectionView: mockCollectionView)
        _ = mockCollectionView.cellForItem(at: IndexPath(item: 0, section: 0))
        XCTAssertTrue(mockCollectionView.cellGotDequeued)
    }

    func test_CellForItem_CallsConfigCell() {
        let mockCollectionView = MockCollectionView.mockCollectionView(with: sut!)
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieManager?.movieResults?.append(movie1) ?? (sut?.movieManager?.movieResults? = [movie1]))
        collectionViewReloadDataAndLayoutIfNeeded(collectionView: mockCollectionView)
        let cell = mockCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? MockMovieResultCell
        XCTAssertEqual(movie1, cell?.catchedItem)
    }

    func test_DidSelectItem_SendsNotification() {
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieManager?.movieResults?.append(movie1) ?? (sut?.movieManager?.movieResults? = [movie1]))
        
        expectation(forNotification: movieResultSelectedNotification, object: nil) { (notification) -> Bool in
            guard let index = notification.userInfo?[indexInfo] as? Int else { return false }
            return index == 0
        }
        collectionView?.delegate?.collectionView!(collectionView!, didSelectItemAt: IndexPath(item: 0, section: 0))
        waitForExpectations(timeout: 3, handler: nil)
    }
    
    func collectionViewReloadDataAndLayoutIfNeeded(collectionView: UICollectionView) {
        collectionView.reloadData()
        collectionView.layoutIfNeeded()
    }
}

extension MovieResultsDataProviderTests {
    class MockCollectionView: UICollectionView {
        var cellGotDequeued = false

        override func dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        }

        class func mockCollectionView(with dataSource: UICollectionViewDataSource & UICollectionViewDelegate) -> MockCollectionView {

            let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
            layout.itemSize = CGSize(width: 60, height: 60)

            let rect = CGRect(x: 50, y: 50, width: 50, height: 50)

            let mockCollectionView = MockCollectionView(frame: rect, collectionViewLayout: layout)
            mockCollectionView.dataSource = dataSource
            mockCollectionView.delegate = dataSource
            mockCollectionView.register(MockMovieResultCell.self, forCellWithReuseIdentifier: movieResultCellIdentifier)
            return mockCollectionView
        }
    }

    class MockMovieResultCell: MovieResultCell {
        var catchedItem: Movie?

        override func configCell(with movieResult: Movie) {
            catchedItem = movieResult
        }
    }
}
