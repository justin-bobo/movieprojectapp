//
//  MovieResultsViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/26/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
import AnimatedCollectionViewLayout
@testable import MovieProjectApp

class MovieResultsViewControllerTests: XCTestCase {
    
    var sut: MovieResultsViewController?
    var findMoviesViewController: FindMoviesViewController?
    var movieManager: MovieManager?
    var movieDetailViewController: MovieDetailViewController?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: movieResultsViewControllerIdentifier) as? MovieResultsViewController
        _ = sut?.view
        findMoviesViewController = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: findMoviesViewControllerIdentifier) as? FindMoviesViewController
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_CollectionViewIsNotNilAfterViewDidLoad() {
        XCTAssertNotNil(sut?.collectionView)
    }

    func test_LoadingView_SetsCollectionViewDataSource() {
        XCTAssertTrue(sut?.collectionView?.dataSource is MovieResultsDataProvider)
    }

    func test_LoadingView_SetsCollectionViewDelegate() {
        XCTAssertTrue(sut?.collectionView?.delegate is MovieResultsDataProvider)
    }

    func test_LoadingView_SetsDataSourceAndDelegateToSameObject() {
        XCTAssertEqual(sut?.collectionView?.dataSource as? MovieResultsDataProvider, sut?.collectionView?.delegate as? MovieResultsDataProvider)
    }

    func test_ViewDidLoad_SetsMovieManagerToManagerOfDataProvider() {
        XCTAssertTrue(sut?.movieManager === sut?.dataProvider?.movieManager)
    }
    
    func test_ViewDidLoad_SetsCollectionViewLayoutToAnimatedCollectionViewLayout() {
        XCTAssertTrue(sut?.collectionView?.collectionViewLayout is AnimatedCollectionViewLayout)
    }

    func test_ItemSelectedNotification_PushesDetailVCAndSetsSaveButton() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        _ = sut?.viewDidAppear(true) // we access the viewDidAppear property of sut to trigger viewDidAppear() because we assume that sut is added as an observer to NotificationCenter.default in viewDidAppear()
        sut?.movieManager = MovieManager.sharedInstance // need to instantiate moviemanager because it's normally instantiated by the findmoviesviewcontroller
        sut?.movieManager?.movieResults = [Movie]()
        sut?.movieManager?.movieResults?.append(Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: ""))

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: movieResultSelectedNotification), object: self, userInfo: [indexInfo: 0])

        guard let movieDetailViewController = mockNavigationController.pushedViewController as? MovieDetailViewController else { XCTFail(); return }

        guard let movieManager = movieDetailViewController.movieInfo?.0 else { XCTFail(); return }

        guard let index = movieDetailViewController.movieInfo?.1 else { XCTFail(); return }

        _ = movieDetailViewController.view

        XCTAssertNotNil(movieDetailViewController.titleLabel)
        XCTAssertNotNil(movieDetailViewController.saveButtonType)
        XCTAssertTrue(movieManager == sut?.movieManager)
        XCTAssertEqual(index, 0)
    }

    func test_ItemSelectedNotification_PushesDetailVCWithMovieResultAndSetsSaveButton() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        _ = sut?.viewDidAppear(true) // we access the viewDidAppear property of sut to trigger viewDidAppear() because we assume that sut is added as an observer to NotificationCenter.default in viewDidAppear()
        sut?.movieManager = MovieManager.sharedInstance // need to instantiate moviemanager because it's normally instantiated by the findmoviesviewcontroller
        sut?.movieManager?.movieResults = [Movie]()

        let movie1 = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 1, title: "Obama", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")

        sut?.movieManager?.movieResults?.append(movie1)
        sut?.movieManager?.saveMovie(movie: movie2)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: movieResultSelectedNotification), object: self, userInfo: [indexInfo: 0])

        guard let movieDetailViewController = mockNavigationController.pushedViewController as? MovieDetailViewController else { XCTFail(); return }

        guard let movieManager = movieDetailViewController.movieInfo?.0 else { XCTFail(); return }

        guard let index = movieDetailViewController.movieInfo?.1 else { XCTFail(); return }

        _ = movieDetailViewController.view
        // need to trigger viewWillAppear because that's where the labels are set
        _ = movieDetailViewController.beginAppearanceTransition(true, animated: true)
        _ = movieDetailViewController.endAppearanceTransition()

        XCTAssertNotNil(movieDetailViewController.titleLabel)
        XCTAssertNotNil(movieDetailViewController.saveButtonType)
        XCTAssertEqual(movieDetailViewController.titleLabel?.text, movie1.title)
        XCTAssertTrue(movieManager == sut?.movieManager)
        XCTAssertEqual(index, 0)
    }

    func test_ItemSelectedNotification_PushesDetailVCAndSetsAlertAndAction() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        _ = sut?.viewDidAppear(true) // we access the view property of sut to trigger viewDidAppear() because we assume that sut is added as an observer to NotificationCenter.default in viewDidAppear()
        sut?.movieManager = MovieManager.sharedInstance // need to instantiate moviemanager because it's normally instantiated by the findmoviesviewcontroller
        sut?.movieManager?.movieResults = [Movie]()
        
        let movie1 = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")

        sut?.movieManager?.movieResults?.append(movie1)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: movieResultSelectedNotification), object: self, userInfo: [indexInfo: 0])
        
        guard let movieDetailViewController = mockNavigationController.pushedViewController as? MovieDetailViewController else { XCTFail(); return }
        
        _ = movieDetailViewController.view
        
        // need to trigger viewWillAppear because that's where the labels are set
        _ = movieDetailViewController.beginAppearanceTransition(true, animated: true)
        _ = movieDetailViewController.endAppearanceTransition()
        
        XCTAssertNotNil(movieDetailViewController.alert)
        XCTAssertNotNil(movieDetailViewController.alertAction)
        XCTAssertEqual(movieDetailViewController.alert?.title, "Movie Saved")
        XCTAssertEqual(movieDetailViewController.alertAction?.title, "Ok")
        XCTAssertEqual(movieDetailViewController.alert?.actions.count, 1)
    }

    func test_ItemSelectedNotification_DoesNotPushDetailVCWhenObserverRemoved() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        
        sut?.movieManager = MovieManager.sharedInstance // need to instantiate moviemanager because it's normally instantiated by the findmoviesviewcontroller
        sut?.movieManager?.movieResults = [Movie]()
        
        let movie1 = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 1, title: "Obama", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        
        sut?.movieManager?.movieResults?.append(movie1)
        sut?.movieManager?.saveMovie(movie: movie2)
        
        _ = sut?.viewDidDisappear(true)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: movieResultSelectedNotification), object: self, userInfo: [indexInfo: 0])
        
        guard let movieDetailViewController = mockNavigationController.pushedViewController as? MovieDetailViewController else { return }
        
        XCTAssertNil(movieDetailViewController)
    }
}

extension MovieResultsViewControllerTests {
    class MockNavigationController: UINavigationController {

        var pushedViewController: UIViewController?

        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            pushedViewController = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
