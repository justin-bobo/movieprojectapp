//
//  SavedMovieViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/17/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class SavedMovieViewControllerTests: XCTestCase {

    var sut: SavedMovieViewController?
    var findMoviesViewController: FindMoviesViewController?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: savedMovieViewControllerIdentifier) as? SavedMovieViewController
        _ = sut?.view // calls viewDidLoad
        _ = sut?.viewDidAppear(true) // we access the view property of sut to trigger viewDidAppear() because we assume that sut is added as an observer to NotificationCenter.default in viewDidAppear()
        findMoviesViewController = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: findMoviesViewControllerIdentifier) as? FindMoviesViewController
    }

    override func tearDown() {
        sut = nil
        findMoviesViewController = nil
        super.tearDown()
    }

    func test_TableViewIsNotNilAfterViewDidLoad() {
        XCTAssertNotNil(sut?.tableView)
    }

    func test_LoadingView_SetsTableViewDataSource() {
        XCTAssertTrue(sut?.tableView?.dataSource is SavedMovieDataProvider)
    }

    func test_LoadingView_SetsTableViewDelegate() {
        XCTAssertTrue(sut?.tableView?.delegate is SavedMovieDataProvider)
    }

    func test_LoadingView_SetsDataSourceAndDelegateToSameObject() {
        XCTAssertEqual(sut?.tableView?.dataSource as? SavedMovieDataProvider, sut?.tableView?.delegate as? SavedMovieDataProvider)
    }

    func test_ViewDidLoad_SetsMovieManagerToManagerOfDataProvider() {
        XCTAssertTrue(sut?.movieManager === sut?.dataProvider?.movieManager)
    }

    func test_ItemSelectedNotification_PushesDetailVCWithSavedMovieAndSetsSaveButton() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        sut?.movieManager?.movieResults = [Movie]()

        let movie1 = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 1, title: "Obama", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")

        sut?.movieManager?.saveMovie(movie: movie1)
        sut?.movieManager?.movieResults?.append(movie2)

        //need to call findMovieVC which, in viewDidLoad, sets movieResults to nil
        _ = findMoviesViewController?.view

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: savedMovieSelectedNotification), object: self, userInfo: [indexInfo: 0])

        guard let movieDetailViewController = mockNavigationController.pushedViewController as? MovieDetailViewController else { XCTFail(); return }

        guard let movieManager = movieDetailViewController.movieInfo?.0 else { XCTFail(); return }

        guard let index = movieDetailViewController.movieInfo?.1 else { XCTFail(); return }

        _ = movieDetailViewController.view

        // need to trigger viewWillAppear because that's where the labels are set
        _ = movieDetailViewController.beginAppearanceTransition(true, animated: true)
        _ = movieDetailViewController.endAppearanceTransition()

        XCTAssertNotNil(movieDetailViewController.titleLabel)
        XCTAssertEqual(movieDetailViewController.titleLabel?.text, movie1.title)
        XCTAssertNotNil(movieDetailViewController.saveButtonType)
        XCTAssertTrue(movieManager == sut?.movieManager)
        XCTAssertEqual(index, 0)
    }
    
    func test_ItemSelectedNotification_PushesDetailVCAndSetsAlertAndAction() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        sut?.movieManager?.movieResults = [Movie]()
        
        let movie1 = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 1, title: "Obama", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        
        sut?.movieManager?.saveMovie(movie: movie1)
        sut?.movieManager?.movieResults?.append(movie2)
        
        //need to call findMovieVC which, in viewDidLoad, sets movieResults to nil
        _ = findMoviesViewController?.view
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: savedMovieSelectedNotification), object: self, userInfo: [indexInfo: 0])
        
        guard let movieDetailViewController = mockNavigationController.pushedViewController as? MovieDetailViewController else { XCTFail(); return }
        
        _ = movieDetailViewController.view
        
        // need to trigger viewWillAppear because that's where the labels are set
        _ = movieDetailViewController.beginAppearanceTransition(true, animated: true)
        _ = movieDetailViewController.endAppearanceTransition()

        XCTAssertNotNil(movieDetailViewController.alert)
        XCTAssertNotNil(movieDetailViewController.alertAction)
        XCTAssertEqual(movieDetailViewController.alert?.title, "Movie Removed")
        XCTAssertEqual(movieDetailViewController.alertAction?.title, "Ok")
        XCTAssertEqual(movieDetailViewController.alert?.actions.count, 1)
    }

    func test_ItemSelectedNotification_DoesNotPushDetailVCWhenObserverRemoved() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        
        let movie1 = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        
        sut?.movieManager?.saveMovie(movie: movie1)
        
        _ = sut?.viewDidDisappear(true)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: savedMovieSelectedNotification), object: self, userInfo: [indexInfo: 0])
        
        guard let movieDetailViewController = mockNavigationController.pushedViewController as? MovieDetailViewController else { XCTAssertTrue(1 == 1); return }
        
        XCTAssertNil(movieDetailViewController)
    }
    
    func test_RemoveMovieFunctionRemovesMovieFromSavedMovies() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        
        let movie1 = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        
        sut?.movieManager?.saveMovie(movie: movie1)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: savedMovieSelectedNotification), object: self, userInfo: [indexInfo: 0])
        
        guard let movieDetailViewController = mockNavigationController.pushedViewController as? MovieDetailViewController else { XCTFail(); return }
        
        _ = movieDetailViewController.view
        
        // need to trigger viewWillAppear because that's where the labels are set
        _ = movieDetailViewController.beginAppearanceTransition(true, animated: true)
        _ = movieDetailViewController.endAppearanceTransition()
        
        movieDetailViewController.movie = movie1
        
        movieDetailViewController.removeMovie()
        
        XCTAssertEqual(sut?.movieManager?.savedMovieCount, 0)
    }
}

extension SavedMovieViewControllerTests {
    class MockNavigationController: UINavigationController {
        var pushedViewController: UIViewController?

        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            pushedViewController = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
