//
//  SavedMovieCellTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/18/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class SavedMovieCellTests: XCTestCase {

    var controller: SavedMovieViewController?
    var tableView: UITableView?
    var cell: SavedMovieCell?

    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: savedMovieViewControllerIdentifier) as? SavedMovieViewController
        _ = controller?.view
        tableView = controller?.tableView
        let dataSource = FakeDataSource()
        tableView?.dataSource = dataSource
        tableView?.delegate = dataSource
        cell = tableView?.dequeueReusableCell(withIdentifier: savedMovieCellIdentifier, for: IndexPath(row: 0, section: 0)) as? SavedMovieCell
    }

    override func tearDown() {
        controller = nil
        tableView = nil
        cell = nil
        super.tearDown()
    }

    func test_HasLabelsAndPosterImage() {
        XCTAssertNotNil(cell?.titleLabel)
        XCTAssertNotNil(cell?.releaseYearLabel)
        XCTAssertNotNil(cell?.ratingLabel)
        XCTAssertNotNil(cell?.categoryLabel)
        XCTAssertNotNil(cell?.runtimeLabel)
        XCTAssertNotNil(cell?.posterImage)
    }

    func test_ConfigCell_SetsLabelsAndPosterImage() {
        let savedMovie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "", rating: "", category: "Comedy", cast: "Donald Trump", director: "Donald Trump", summary: "Trump says something dumb", runtime: "90 min", poster: "trump_sampleposter")
        cell?.configCell(with: savedMovie)
        XCTAssertEqual(savedMovie.title, cell?.titleLabel?.text)
        XCTAssertEqual(String(savedMovie.releaseYear), cell?.releaseYearLabel?.text)
        XCTAssertEqual(String(savedMovie.rating), cell?.ratingLabel?.text)
        XCTAssertEqual(savedMovie.category, cell?.categoryLabel?.text)
        XCTAssertEqual(savedMovie.runtime, cell?.runtimeLabel?.text)
        XCTAssertNotNil(cell?.posterImage?.image)
    }
}

extension SavedMovieCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        // not being called in our tests so it's ok to return UITableViewCell()
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return UITableViewCell()
        }
    }
}
