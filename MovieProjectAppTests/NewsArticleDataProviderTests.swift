//
//  NewsArticleDataProviderTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class NewsArticleDataProviderTests: XCTestCase {

    var sut: NewsArticleDataProvider?
    var controller: NewsArticleViewController?
    var tableView: UITableView?

    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: newsArticleViewControllerIdentifier) as? NewsArticleViewController
        sut = controller?.dataProvider as? NewsArticleDataProvider
        sut?.newsArticleManager = NewsArticleManager()
        _ = controller?.view
        tableView = controller?.tableView
    }

    override func tearDown() {
        sut = nil
        controller = nil
        tableView = nil
        super.tearDown()
    }

    func test_NumberOfSections_IsOne() {
        let numberOfSections = tableView?.numberOfSections
        XCTAssertEqual(numberOfSections, 1)
    }
    
    func test_NumberOfRowsEqualsZeroWhenNewsArticleManagerIsNil() {
        sut?.newsArticleManager = nil
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 0)
    }

    func test_NumberOfRows_IsNewsArticlesCount() {
        let newsArticle1 = NewsArticle(title: "1", description: "", url: "", imageUrl: "")
        let newsArticle2 = NewsArticle(title: "2", description: "", url: "", imageUrl: "")
        sut?.newsArticleManager?.add(newsArticle: newsArticle1)
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 1)
        sut?.newsArticleManager?.add(newsArticle: newsArticle2)
        tableView?.reloadData() // have to add reload data because tableview caches values returned from numberOfRows function. It's a performance optimization for tableviews. We tell the tableview the datasource has changed by calling reloadData()
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 2)
    }

    func test_CellForRow_ReturnsCustomNewsArticleCell() {
        let newsArticle = NewsArticle(title: "1", description: "", url: "", imageUrl: "")
        sut?.newsArticleManager?.add(newsArticle: newsArticle)
        tableView?.reloadData()
        let cell = tableView?.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is NewsArticleCell)
    }

    func test_CellForRow_DequeuesCellFromTableView() {
        let mockTableView = MockTableView.mockTableView(withDataSource: sut!)
        let newsArticle = NewsArticle(title: "1", description: "", url: "", imageUrl: "")
        sut?.newsArticleManager?.add(newsArticle: newsArticle)
        mockTableView.reloadData()
        _ = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(mockTableView.cellGotDequeued)
    }

    func test_CellForRow_CallsConfigCell() {
        let mockTableView = MockTableView.mockTableView(withDataSource: sut!)
        let newsArticle = NewsArticle(title: "1", description: "", url: "", imageUrl: "")
        sut?.newsArticleManager?.add(newsArticle: newsArticle)
        mockTableView.reloadData()
        let cell = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? MockNewsArticleCell
        XCTAssertEqual(newsArticle, cell?.catchedItem)
    }

    func test_DidSelectRow_SendsNotification() {
        let newsArticle = NewsArticle(title: "1", description: "", url: "", imageUrl: "")
        sut?.newsArticleManager?.add(newsArticle: newsArticle)

        expectation(forNotification: newsArticleSelectedNotification, object: nil) { (notification) -> Bool in
            guard let index = notification.userInfo?[indexInfo] as? Int else { return false }
            return index == 0
        }

        tableView?.delegate?.tableView!(tableView!, didSelectRowAt: IndexPath(row: 0, section: 0))
        waitForExpectations(timeout: 3, handler: nil)
    }
}

extension NewsArticleDataProviderTests {
    class MockTableView: UITableView {
        var cellGotDequeued = false

        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }

        class func mockTableView(withDataSource dataSource: UITableViewDataSource & UITableViewDelegate) -> MockTableView {
            let mockTableView = MockTableView()
            mockTableView.dataSource = dataSource
            mockTableView.delegate = dataSource
            mockTableView.register(MockNewsArticleCell.self, forCellReuseIdentifier: newsArticleCellIdentifier)
            return mockTableView
        }

    }
    
    class MockNewsArticleCell: NewsArticleCell {
        var catchedItem: NewsArticle?
        
        override func configCell(with newsArticle: NewsArticle) {
            catchedItem = newsArticle
        }
    }
}
