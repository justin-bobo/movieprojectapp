//
//  FileMovieDetailViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/9/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
import AVKit
import AVFoundation
@testable import MovieProjectApp

class FileMovieDetailViewControllerTests: XCTestCase {
    
    var sut: FileMovieDetailViewController?
    
    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: fileMovieDetailViewControllerIdentifier) as? FileMovieDetailViewController
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_ViewDidLoad_SetsURL() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let userFileManager = UserFileManager()
        userFileManager.add(file: file)
        sut?.fileInfo = (userFileManager, 0)
        _ = sut?.view
        
        XCTAssertNotNil(sut?.url)
    }
    
    func test_ViewDidLoad_SetsAVPlayer() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let userFileManager = UserFileManager()
        userFileManager.add(file: file)
        sut?.fileInfo = (userFileManager, 0)
        _ = sut?.view

        XCTAssertNotNil(sut?.player)
    }
}
