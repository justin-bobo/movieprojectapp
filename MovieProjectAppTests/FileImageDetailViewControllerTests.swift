//
//  FileImageDetailViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/9/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class FileImageDetailViewControllerTests: XCTestCase {

    var sut: FileImageDetailViewController?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: fileImageDetailViewControllerIdentifier) as? FileImageDetailViewController
        _ = sut?.view
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_ViewDidLoad_SetsScrollViewZoomScale() {
        XCTAssertEqual(sut?.scrollView?.minimumZoomScale, 1.0)
        XCTAssertEqual(sut?.scrollView?.maximumZoomScale, 6.0)
    }
}
