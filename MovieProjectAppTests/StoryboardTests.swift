//
//  StoryboardTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/19/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class StoryboardTests: XCTestCase {

    var tabBarController: UITabBarController?

    override func setUp() {
        super.setUp()
        tabBarController = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController
    }

    override func tearDown() {
        tabBarController = nil
        super.tearDown()
    }

    func test_InitialViewController_isLoginViewController() {
        guard let _ = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateInitialViewController() as? LoginViewController else { XCTFail(); return }
    }

    // for now, test that initial VC is the savedMoviesVC
    func test_InitialViewControllerInTabBarController_isUINavigationController() {
        let rootViewController = tabBarController?.viewControllers?[0]
        XCTAssertTrue(rootViewController is UINavigationController)
    }

    func test_SecondViewController_inFirstNavigationController_isNewsArticleViewController() {
        let rootViewController = tabBarController?.viewControllers?[0] as? UINavigationController
        let rootOfRootViewController = rootViewController?.viewControllers[0]
        XCTAssertTrue(rootOfRootViewController is SavedMovieViewController)
    }

    func test_SecondViewController_inTabBarController_isUINavigationController() {
        let rootViewController = tabBarController?.viewControllers?[1]
        XCTAssertTrue(rootViewController is UINavigationController)
    }

    func test_SecondViewController_inSecondNavigationController_isNewsArticleViewController() {
        let rootViewController = tabBarController?.viewControllers?[1] as? UINavigationController
        let rootOfRootViewController = rootViewController?.viewControllers[0]
        XCTAssertTrue(rootOfRootViewController is NewsArticleViewController)
    }
    
    func test_ThirdViewController_inTabBarController_isUINavigationController() {
        let rootViewController = tabBarController?.viewControllers?[2]
        XCTAssertTrue(rootViewController is UINavigationController)
    }
    
    func test_SecondViewController_inThirdNavigationController_isFindMoviesViewController() {
        let rootViewController = tabBarController?.viewControllers?[2] as? UINavigationController
        let rootOfRootViewController = rootViewController?.viewControllers[0]
        XCTAssertTrue(rootOfRootViewController is FindMoviesViewController)
    }
    
    func test_FourthViewController_inTabBarController_isUINavigationController() {
        let rootViewController = tabBarController?.viewControllers?[3]
        XCTAssertTrue(rootViewController is UINavigationController)
    }
    
    func test_SecondViewController_inFourthNavigationController_isFileViewController() {
        let rootViewController = tabBarController?.viewControllers?[3] as? UINavigationController
        let rootOfRootViewController = rootViewController?.viewControllers[0]
        XCTAssertTrue(rootOfRootViewController is FileViewController)
    }
}
