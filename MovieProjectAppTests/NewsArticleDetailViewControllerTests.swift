//
//  NewsArticleDetailViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/21/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
import WebKit
@testable import MovieProjectApp

class NewsArticleDetailViewControllerTests: XCTestCase {

    var sut: NewsArticleDetailViewController?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: newsArticleDetailViewControllerIdentifier) as? NewsArticleDetailViewController
        _ = sut?.view
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_NewsArticleDetailViewController_InstantiatesWebView() {
        XCTAssertNotNil(sut?.webView)
    }

    func test_NewsArticleDetailViewController_SetsWebViewDelegateToSUT() {
        XCTAssertTrue(sut?.webView?.navigationDelegate is NewsArticleDetailViewController)
    }

    func test_NewsArticleDetailViewController_View_IsWebView() {
        XCTAssertTrue(sut?.view == sut?.webView)
    }

    func test_AddingNewsArticleToArray_SetsURLToURLVariableandURLOfActiveWebView() {
        let newsArticle = NewsArticle(title: "Trump is Dumb", description: "Trump is not very smart", url: "https://www.apple.com/", imageUrl: "https://en.wikipedia.org/wiki/Apple_Inc.#/media/File:Apple_Headquarters_in_Cupertino.jpg")
        let newsArticleManager = NewsArticleManager()
        newsArticleManager.add(newsArticle: newsArticle)
        sut?.newsArticleInfo = (newsArticleManager, 0)

        sut?.beginAppearanceTransition(true, animated: true)
        sut?.endAppearanceTransition()

        XCTAssertEqual(sut?.url, sut?.webView?.url)
    }
}
