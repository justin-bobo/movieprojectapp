//
//  FileTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class FileTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_FileIsNotNil() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        XCTAssertNotNil(file)
    }
    
    func test_Files_AreEqual() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let file2 = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        XCTAssertEqual(file, file2)
    }
    
    func test_FileIds_AreNotEqual() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "0", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let file2 = File(id: "1", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        XCTAssertNotEqual(file, file2)
    }
    
    func test_FileTitles_AreNotEqual() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let file2 = File(id: "", title: "Sample title 2", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        XCTAssertNotEqual(file, file2)
    }
}
