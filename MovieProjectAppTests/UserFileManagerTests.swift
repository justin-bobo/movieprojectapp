//
//  UserFileManagerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class UserFileManagerTests: XCTestCase {

    var sut: UserFileManager?

    override func setUp() {
        super.setUp()
        sut = UserFileManager()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_SavedFilesCount_IsInitiallyZero() {
        let savedFilesCount = sut?.savedFilesCount
        XCTAssertEqual(savedFilesCount, 0)
    }

    func test_SavedFilesCount_EqualsOne_WhenFileAdded() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.add(file: file)
        let savedFilesCount = sut?.savedFilesCount
        XCTAssertEqual(savedFilesCount, 1)
    }

    func test_SavedFilesCount_EqualsZero_WhenFileAddedAndDeleted() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.add(file: file)
        sut?.remove(at: 0)
        let savedFilesCount = sut?.savedFilesCount
        XCTAssertEqual(savedFilesCount, 0)
    }

    func test_SavedFileAt_AfterAddingSavedFile_ReturnsThatSavedFile() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.add(file: file)
        let returnedFile = sut?.returnFile(at: 0)
        XCTAssertEqual(file, returnedFile)
    }

    func test_SavedFileAt_AfterAddingTwoSavedFiles_ReturnsSecondFile() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let file2 = File(id: "", title: "Sample title 2", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.add(file: file)
        sut?.add(file: file2)
        let returnedFile = sut?.returnFile(at: 1)
        XCTAssertEqual(file2, returnedFile)
    }

    func test_AttemptingToAddSameFileTwice_OnlyAddsItOnce() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let file2 = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.add(file: file)
        sut?.add(file: file2)
        XCTAssertEqual(sut?.savedFilesCount, 1)
    }
}
