//
//  SavedMovieDataProviderTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/17/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class SavedMovieDataProviderTests: XCTestCase {

    var sut: SavedMovieDataProvider?
    var controller: SavedMovieViewController?
    var tableView: UITableView?

    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: savedMovieViewControllerIdentifier) as? SavedMovieViewController
        sut = controller?.dataProvider as? SavedMovieDataProvider
        _ = controller?.view
        sut?.movieManager = controller?.movieManager
        sut?.movieManager?.savedMovies = [Movie]()
        tableView = controller?.tableView
        tableView?.dataSource = sut
        tableView?.delegate = sut
    }

    override func tearDown() {
        sut = nil
        controller = nil
        tableView = nil
        super.tearDown()
    }

    func test_NumberOfSectionsEqualsOne() {
        let numberOfSections = controller?.tableView?.numberOfSections
        XCTAssertEqual(numberOfSections, 1)
    }

    func test_NumberOfRowsEqualsMovieManagerCount() {
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 1, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.movieManager?.saveMovie(movie: movie1)
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 1)
        sut?.movieManager?.saveMovie(movie: movie2)
        tableView?.reloadData()
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 2)
    }

    func test_NumberOfRowsEqualsOneWhenMovieManagerIsNil() {
        sut?.movieManager = nil
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 0)
    }

    func test_CellForRow_ReturnsCustomSavedMovieCell() {
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.movieManager?.saveMovie(movie: movie1)
        tableView?.reloadData()
        let cell = tableView?.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is SavedMovieCell)
    }
    // want to know that the cellForRow function is calling dequeueReusableCell
    func test_CellForRow_DequeuesCellFromTableView() {
        let mockTableView = MockTableView.mockTableView(with: sut!)
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.movieManager?.saveMovie(movie: movie1)
        mockTableView.reloadData()
        _ = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(mockTableView.cellGotDequeued)
    }
    // want to know that the cellForRow function is calling configCell
    func test_CellForRow_CallsConfigCell() {
        let mockTableView = MockTableView.mockTableView(with: sut!)
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.movieManager?.saveMovie(movie: movie1)
        mockTableView.reloadData()
        let cell = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? MockSavedMovieCell
        XCTAssertEqual(movie1, cell?.catchedItem)
    }
    // need to test that when movie (in table view row is selected), send the notification with the tapped row in the userInfo - this test is currently causing an index out of range error
    func test_DidSelectRow_SendsNotification() {
        let movie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "", rating: "", category: "Comedy", cast: "Donald Trump", director: "Arnold S", summary: "Donald Trump is an idiot", runtime: "90 min", poster: "image url placeholder")
        sut?.movieManager?.saveMovie(movie: movie)

        expectation(forNotification: savedMovieSelectedNotification, object: nil) { (notification) -> Bool in
            guard let index = notification.userInfo?[indexInfo] as? Int else { return false }
            return index == 0
        }

        tableView?.delegate?.tableView!(tableView!, didSelectRowAt: IndexPath(row: 0, section: 0))
        waitForExpectations(timeout: 3, handler: nil)
    }
}

extension SavedMovieDataProviderTests {
    class MockTableView: UITableView {
        var cellGotDequeued = false

        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }

        class func mockTableView(with dataSource: UITableViewDataSource & UITableViewDelegate) -> MockTableView {
            let mockTableView = MockTableView()
            mockTableView.dataSource = dataSource
            mockTableView.register(MockSavedMovieCell.self, forCellReuseIdentifier: savedMovieCellIdentifier)
            return mockTableView
        }
    }

    class MockSavedMovieCell: SavedMovieCell {
        var catchedItem: Movie?

        override func configCell(with savedMovie: Movie) {
            catchedItem = savedMovie
        }
    }
}
