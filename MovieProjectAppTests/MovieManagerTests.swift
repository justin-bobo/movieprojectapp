//
//  MovieManagerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class MovieManagerTests: XCTestCase {

    var sut: MovieManager?

    override func setUp() {
        super.setUp()
        sut = MovieManager()
        sut?.savedMovies = [Movie]()
        sut?.movieResults = [Movie]()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_SavedMovieCount_IsInitiallyZero() {
        XCTAssertEqual(sut?.savedMovieCount, 0)
    }

    func test_MovieResultsCount_IsInitiallyZero() {
        XCTAssertEqual(sut?.movieResultsCount, 0)
    }

    func test_MovieResultsCount_IsZero_WhenArrayIsNil() {
        sut?.movieResults = nil
        XCTAssertEqual(sut?.movieResultsCount, 0)
    }

    func test_SavedMovieCount_EqualsOne_WhenMovieAdded() {
        let movie = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.saveMovie(movie: movie)
        XCTAssertEqual(sut?.savedMovieCount, 1)
    }

    func test_MovieResultsCount_EqualsOne_WhenMovieAdded() {
        let movie = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieResults?.append(movie) ?? (sut?.movieResults? = [movie]))
        XCTAssertEqual(sut?.movieResultsCount, 1)
    }

    func test_SavedMovieCount_EqualsZero_WhenMovieAddedAndDeleted() {
        let movie = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.saveMovie(movie: movie)
        sut?.removeSavedMovie(at: 0)
        XCTAssertEqual(sut?.savedMovieCount, 0)
    }

    func test_SavedMovieAt_AfterAddingMovie_ReturnsThatMovie() {
        let movie = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.saveMovie(movie: movie)
        let returnedMovie = sut?.returnSavedMovie(at: 0)
        XCTAssertEqual(returnedMovie, movie)
    }

    func test_MovieResultsAt_AfterAddingMovie_ReturnsThatMovie() {
        let movie = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieResults?.append(movie) ?? (sut?.movieResults? = [movie]))
        let returnedMovie = sut?.returnMovieResult(at: 0)
        XCTAssertEqual(returnedMovie, movie)
    }

    func test_SavedMovieAt_AfterAddingTwoMovies_ReturnsSecondMovie() {
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 1, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.saveMovie(movie: movie1)
        sut?.saveMovie(movie: movie2)
        let returnedMovie = sut?.returnSavedMovie(at: 1)
        XCTAssertEqual(movie2, returnedMovie)
    }

    func test_MovieResultsAt_AfterAddingTwoMovies_ReturnsSecondMovie() {
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 1, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        (sut?.movieResults?.append(movie1) ?? (sut?.movieResults? = [movie1]))
        (sut?.movieResults?.append(movie2) ?? (sut?.movieResults? = [movie2]))
        let returnedMovie = sut?.returnMovieResult(at: 1)
        XCTAssertEqual(movie2, returnedMovie)
    }

    func test_AttemptingToAddSameSavedMovieTwice_OnlyAddsItOnce() {
        let movie1 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        let movie2 = Movie(id: 0, title: "", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        sut?.saveMovie(movie: movie1)
        sut?.saveMovie(movie: movie2)
        XCTAssertEqual(sut?.savedMovieCount, 1)
    }
}
