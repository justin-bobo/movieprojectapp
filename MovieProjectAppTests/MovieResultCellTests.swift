//
//  MovieResultCellTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/30/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class MovieResultCellTests: XCTestCase {
    
    var controller: MovieResultsViewController?
    var collectionView: UICollectionView?
    var cell: MovieResultCell?

    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: "MovieResultsViewController") as? MovieResultsViewController
        _ = controller?.view
        collectionView = controller?.collectionView
        let dataSource = FakeDataSource()
        collectionView?.dataSource = dataSource
        collectionView?.delegate = dataSource
        cell = collectionView?.dequeueReusableCell(withReuseIdentifier: movieResultCellIdentifier, for: IndexPath(item: 0, section: 0)) as? MovieResultCell
    }

    override func tearDown() {
        controller = nil
        collectionView = nil
        cell = nil
        super.tearDown()
    }

    func test_HasLabelsAndImage() {
        XCTAssertNotNil(cell?.titleLabel)
        XCTAssertNotNil(cell?.releaseYearLabel)
        XCTAssertNotNil(cell?.ratingLabel)
        XCTAssertNotNil(cell?.categoryLabel)
        XCTAssertNotNil(cell?.posterImage)
    }

    func test_ConfigCell_SetsLabelsAndImage() {
        let movie = Movie(id: 0, title: "Trump", releaseYear: "2016", rating: "0.0", category: "Comedy", cast: "Donald Trump", director: "Sean Spicer", summary: "Trump is a moron", runtime: "90 min", poster: "http://i2.cdn.cnn.com/cnnnext/dam/assets/150827102252-donald-trump-july-10-2015-super-169.jpg")
        cell?.configCell(with: movie)
        XCTAssertEqual(cell?.titleLabel?.text, movie.title)
        XCTAssertEqual(cell?.releaseYearLabel?.text, movie.releaseYear)
        XCTAssertEqual(cell?.ratingLabel?.text, movie.rating)
        XCTAssertEqual(cell?.categoryLabel?.text, movie.category)
        XCTAssertNotNil(cell?.posterImage?.image)
    }
}

extension MovieResultCellTests {
    class FakeDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            return UICollectionViewCell()
        }
    }
}
