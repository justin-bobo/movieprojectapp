//
//  FileViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
import MobileCoreServices
import PDFReader
@testable import MovieProjectApp

class FileViewControllerTests: XCTestCase {

    var sut: FileViewController?
    var dataProvider: FileDataProvider?
    var tableView: UITableView?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: fileViewControllerIdentifier) as? FileViewController
        dataProvider = sut?.dataProvider as? FileDataProvider
        dataProvider?.userFileManager = UserFileManager()
        _ = sut?.view
        tableView = sut?.tableView
    }

    override func tearDown() {
        sut = nil
        tableView = nil
        super.tearDown()
    }

    func test_TableViewIsNotNilAfterViewDidLoad() {
        XCTAssertNotNil(sut?.tableView)
    }

    func test_LoadingView_SetsTableViewDataSource() {
        XCTAssertTrue(sut?.tableView?.dataSource is FileDataProvider)
    }

    func test_LoadingView_SetsTableViewDelegate() {
        XCTAssertTrue(sut?.tableView?.delegate is FileDataProvider)
    }

    func test_LoadingView_SetsDataSourceAndDelegateToSameObject() {
        XCTAssertEqual(sut?.tableView?.dataSource as? FileDataProvider, sut?.tableView?.delegate as? FileDataProvider)
    }

    func test_ViewDidLoad_SetsFileManagerToManagerOfDataProvider() {
        XCTAssertTrue(sut?.userFileManager === sut?.dataProvider?.userFileManager)
    }

    func test_FileSelected_PushesFileDefaultDetailViewController() {
        sut?.userFileManager?.savedFiles.removeAll()
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        _ = sut?.view // addObserver called in viewDidLoad
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        sut?.userFileManager?.savedFiles.append(File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!))
        sut?.fileName = sut?.userFileManager?.returnFile(at: 0).title

        NotificationCenter.default.post(name: NSNotification.Name(fileSelectedNotification), object: nil, userInfo: [indexInfo: 0])
        guard let fileDetailViewController = mockNavigationController.pushedViewController as? FileDefaultDetailViewController else { XCTFail(); return }
        guard let userFileManager = fileDetailViewController.fileInfo?.0 else { XCTFail(); return }
        guard let index = fileDetailViewController.fileInfo?.1 else { XCTFail(); return }

        XCTAssertTrue(userFileManager === sut?.userFileManager)
        XCTAssertEqual(index, 0)
    }
    
    func test_ImageFileSelected_PushesFileImageDetailViewController() {
        sut?.userFileManager?.savedFiles.removeAll()
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        _ = sut?.view // addObserver called in viewDidLoad
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        sut?.userFileManager?.savedFiles.append(File(id: "", title: "Image.jpeg", data: fakeData!, url: URL(string: "https://www.apple.com/")!))
        sut?.fileName = sut?.userFileManager?.returnFile(at: 0).title
        
        NotificationCenter.default.post(name: NSNotification.Name(fileSelectedNotification), object: nil, userInfo: [indexInfo: 0])
        guard let fileDetailViewController = mockNavigationController.pushedViewController as? FileImageDetailViewController else { XCTFail(); return }
        guard let userFileManager = fileDetailViewController.fileInfo?.0 else { XCTFail(); return }
        guard let index = fileDetailViewController.fileInfo?.1 else { XCTFail(); return }
        
        XCTAssertTrue(userFileManager === sut?.userFileManager)
        XCTAssertEqual(index, 0)
    }

    func test_MovieFileSelected_PushesFileMovieDetailViewController() {
        sut?.userFileManager?.savedFiles.removeAll()
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        _ = sut?.view // addObserver called in viewDidLoad
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        sut?.userFileManager?.savedFiles.append(File(id: "", title: "Movie.mov", data: fakeData!, url: URL(string: "https://www.apple.com/")!))
        sut?.fileName = sut?.userFileManager?.returnFile(at: 0).title
        
        NotificationCenter.default.post(name: NSNotification.Name(fileSelectedNotification), object: nil, userInfo: [indexInfo: 0])
        guard let fileDetailViewController = mockNavigationController.pushedViewController as? FileMovieDetailViewController else { XCTFail(); return }
        guard let userFileManager = fileDetailViewController.fileInfo?.0 else { XCTFail(); return }
        guard let index = fileDetailViewController.fileInfo?.1 else { XCTFail(); return }
        
        XCTAssertTrue(userFileManager === sut?.userFileManager)
        XCTAssertEqual(index, 0)
    }

    func test_PDFFileSelected_PushesFilePDFDetailViewController() {
        sut?.userFileManager?.savedFiles.removeAll()
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        _ = sut?.view // addObserver called in viewDidLoad
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        sut?.userFileManager?.savedFiles.append(File(id: "", title: "PDFExample.pdf", data: fakeData!, url: URL(string: "https://www.apple.com/")!))
        sut?.fileName = sut?.userFileManager?.returnFile(at: 0).title
        sut?.fileURL = URL(string: "http://www.pdf995.com/samples/pdf.pdf")
        
        NotificationCenter.default.post(name: NSNotification.Name(fileSelectedNotification), object: nil, userInfo: [indexInfo: 0])
        guard let _ = mockNavigationController.pushedViewController as? PDFViewController else { XCTFail(); return }
    }

    func test_CallingDidPickDocumentPicker_SetsDelegate() {
        let documentMenu = UIDocumentMenuViewController(documentTypes: ["public.image"], in: .import)
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.image"], in: .import)
        sut?.documentMenu(documentMenu, didPickDocumentPicker: documentPicker)
        XCTAssertTrue(documentPicker.delegate is FileViewController)
    }

    func test_CallingDidPickDocumentPicker_PresentsPicker() {
        let navigationController = UINavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = navigationController

        let documentMenu = UIDocumentMenuViewController(documentTypes: ["public.image"], in: .import)
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.image"], in: .import)
        sut?.documentMenu(documentMenu, didPickDocumentPicker: documentPicker)

        guard let _ = navigationController.presentedViewController as? UIDocumentPickerViewController else { XCTFail(); return }
    }
    
    func test_CallingDidPickDocumentPicker_SetsFileNameToLastPathComponent() {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeCompositeContent as String], in: .import)
        sut?.documentPicker(documentPicker, didPickDocumentAt: URL(string: "http://www.pdf995.com/samples/pdf.pdf")!)
        XCTAssertEqual(sut?.fileName, "pdf.pdf")
    }

    func test_CallingDidPickDocumentAtURL_CreatesFileAndAppendsToFilesArray() {
        sut?.userFileManager?.savedFiles.removeAll()
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeCompositeContent as String], in: .import)
        sut?.documentPicker(documentPicker, didPickDocumentAt: URL(string: "http://www.pdf995.com/samples/pdf.pdf")!)
        XCTAssertEqual(sut?.userFileManager?.savedFilesCount, 1)
    }

    func test_CallingDidFinishPickingMediaWithInfo_DoesNotCreateFileAndAppendToFilesArray() {
        sut?.userFileManager?.savedFiles.removeAll()
        let imagePicker = UIImagePickerController()
        let mediaInfo = ["key": URL(string: "idk") as Any]
        sut?.imagePickerController(imagePicker, didFinishPickingMediaWithInfo: mediaInfo)
        XCTAssertEqual(sut?.userFileManager?.savedFilesCount, 0)
    }

    func test_TappingAddFileButton_CreatesMenuViewController() {
        sut?.addFileTapped(UIBarButtonItem())
        XCTAssertNotNil(sut?.importMenu)
    }

    func test_TappingAddFileButton_SetsMenuViewControllerDelegateToSelf() {
        sut?.addFileTapped(UIBarButtonItem())
        XCTAssertTrue(sut?.importMenu?.delegate is FileViewController)
    }

    func test_TappingAddFileButton_WhenImportMenuNil_DoesntSetDelegate() {
        sut?.addFileTapped(UIBarButtonItem())
        sut?.importMenu = nil
        XCTAssertFalse(sut?.importMenu?.delegate is FileViewController)
    }

    func test_TappingAddFileButton_CreatesImagePickerViewController() {
        sut?.addFileTapped(UIBarButtonItem())
        XCTAssertNotNil(sut?.imagePicker)
    }

    func test_TappingAddFileButton_SetsImagePickerViewControllerDelegateToSelf() {
        sut?.addFileTapped(UIBarButtonItem())
        XCTAssertTrue(sut?.imagePicker?.delegate is FileViewController)
    }

    func test_TappingAddFileButton_WhenImagePickerMenuNil_DoesntSetDelegate() {
        sut?.addFileTapped(UIBarButtonItem())
        sut?.imagePicker = nil
        XCTAssertFalse(sut?.imagePicker?.delegate is FileViewController)
    }

    func test_TappingAddFileButton_SetsMediaTypesForImagePicker() {
        sut?.addFileTapped(UIBarButtonItem())
        XCTAssertTrue((sut?.imagePicker?.mediaTypes)! == [kUTTypeImage as String, kUTTypeMovie as String])
    }

    func test_TappingAddFileButton_SetsAllowsEditingToFalseForImagePicker() {
        sut?.addFileTapped(UIBarButtonItem())
        XCTAssertTrue(sut?.imagePicker?.allowsEditing == false)
    }
}

extension FileViewControllerTests {
    class MockNavigationController: UINavigationController {
        var pushedViewController: UIViewController?

        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            pushedViewController = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
