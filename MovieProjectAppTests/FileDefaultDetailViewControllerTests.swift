//
//  FileDefaultDetailViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class FileDefaultDetailViewControllerTests: XCTestCase {

    var sut: FileDefaultDetailViewController?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: fileDefaultDetailViewControllerIdentifier) as? FileDefaultDetailViewController
        _ = sut?.view
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_FileDefaultDetailViewController_InstantiatesWebView() {
        XCTAssertNotNil(sut?.webView)
    }
    
    func test_FileDefaultDetailViewController_SetsWebViewDelegateToSUT() {
        XCTAssertTrue(sut?.webView?.navigationDelegate is FileDefaultDetailViewController)
    }
    
    func test_FileDefaultDetailViewController_View_IsWebView() {
        XCTAssertTrue(sut?.view == sut?.webView)
    }

    func test_AddingSavedFileSetsTitleOfView() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let userFileManager = UserFileManager()
        userFileManager.add(file: file)
        sut?.fileInfo = (userFileManager, 0)

        sut?.beginAppearanceTransition(true, animated: true)
        sut?.endAppearanceTransition()

        XCTAssertEqual(file.title, sut?.title)
        XCTAssertEqual(sut?.url, sut?.webView?.url)
    }
}
