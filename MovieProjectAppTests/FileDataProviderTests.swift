//
//  FileDataProviderTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class FileDataProviderTests: XCTestCase {
    
    var sut: FileDataProvider?
    var controller: FileViewController?
    var tableView: UITableView?
    
    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: fileViewControllerIdentifier) as? FileViewController
        sut = controller?.dataProvider as? FileDataProvider
        sut?.userFileManager = UserFileManager()
        _ = controller?.view
        tableView = controller?.tableView
    }
    
    override func tearDown() {
        sut = nil
        controller = nil
        tableView = nil
        super.tearDown()
    }
    
    func test_NumberOfSections_IsOne() {
        let numberOfSections = tableView?.numberOfSections
        XCTAssertEqual(numberOfSections, 1)
    }
    
    func test_NumberOfRowsEqualsZeroWhenUserFileManagerIsNil() {
        sut?.userFileManager = nil
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 0)
    }
    
    func test_NumberOfRows_IsSavedFilesCount() {
        sut?.userFileManager?.savedFiles.removeAll()
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let fakeData2 = "{\"fake\": \"data\"}".data(using: .utf8)
        let file1 = File(id: "0", title: "File 1", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        let file2 = File(id: "1", title: "File 2", data: fakeData2!, url: URL(string: "https://www.apple.com/")!)
        sut?.userFileManager?.add(file: file1)
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 1)
        sut?.userFileManager?.add(file: file2)
        tableView?.reloadData() // have to add reload data because tableview caches values returned from numberOfRows function. It's a performance optimization for tableviews. We tell the tableview the datasource has changed by calling reloadData()
        XCTAssertEqual(tableView?.numberOfRows(inSection: 0), 2)
    }
    
    func test_CellForRow_ReturnsCustomFileCell() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file1 = File(id: "", title: "File 1", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.userFileManager?.add(file: file1)
        tableView?.reloadData()
        let cell = tableView?.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is FileCell)
    }
    
    func test_CellForRow_DequeuesCellFromTableView() {
        let mockTableView = MockTableView.mockTableView(withDataSource: sut!)
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file1 = File(id: "", title: "File 1", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.userFileManager?.add(file: file1)
        mockTableView.reloadData()
        _ = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(mockTableView.cellGotDequeued)
    }
    
    func test_CellForRow_CallsConfigCell() {
        let mockTableView = MockTableView.mockTableView(withDataSource: sut!)
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file1 = File(id: "", title: "File 1", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.userFileManager?.add(file: file1)
        mockTableView.reloadData()
        let cell = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? MockFileCell
        XCTAssertEqual(file1, cell?.catchedItem)
    }
    // need test for when article is selected, a modal/segue presents the article in a uiwebview
    func test_DidSelectRow_SendsNotification() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file1 = File(id: "", title: "File 1", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        sut?.userFileManager?.add(file: file1)
        
        expectation(forNotification: fileSelectedNotification, object: nil) { (notification) -> Bool in
            guard let index = notification.userInfo?[indexInfo] as? Int else { return false }
            return index == 0
        }
        
        tableView?.delegate?.tableView!(tableView!, didSelectRowAt: IndexPath(row: 0, section: 0))
        waitForExpectations(timeout: 3, handler: nil)
    }
}

extension FileDataProviderTests {
    class MockTableView: UITableView {
        var cellGotDequeued = false
        
        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }
        
        class func mockTableView(withDataSource dataSource: UITableViewDataSource & UITableViewDelegate) -> MockTableView {
            let mockTableView = MockTableView()
            mockTableView.dataSource = dataSource
            mockTableView.delegate = dataSource
            mockTableView.register(MockFileCell.self, forCellReuseIdentifier: fileCellIdentifier)
            return mockTableView
        }
        
    }
    
    class MockFileCell: FileCell {
        var catchedItem: File?
        
        override func configCell(with file: File) {
            catchedItem = file
        }
    }
}
