//
//  APIClientTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/23/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
import SwiftyJSON
@testable import MovieProjectApp

class MovieAPIClientTests: XCTestCase {

    var sut: APIClient?
    var movieResultsViewController: MovieResultsViewController?

    override func setUp() {
        super.setUp()
        sut = APIClient()
        movieResultsViewController = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: movieResultsViewControllerIdentifier) as? MovieResultsViewController
        movieResultsViewController?.movieManager = MovieManager.sharedInstance
    }

    override func tearDown() {
        sut = nil
        movieResultsViewController = nil
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func test_CreateURLFunctionWithHardcodedURL_ReturnsCorrectURL() {
        let hardcodedURL = URL(string: "https://netflixroulette.net/api/api.php?title=Attack%20on%20Titan")
        let urlWithFunction = sut?.createURLWithComponents(scheme: "https", host: "netflixroulette.net", path: "/api/api.php", queryItems: ["title": "Attack on Titan"])
        XCTAssertEqual(hardcodedURL, urlWithFunction)
    }

    func test_CreateURLFunctionWithURLComponents_ReturnsCorrectURL() {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "netflixroulette.net"
        urlComponents.path = "/api/api.php"
        let yearQueryItem = URLQueryItem(name: "year", value: "2005")
        let titleQueryItem = URLQueryItem(name: "title", value: "The Boondocks")
        urlComponents.queryItems = [yearQueryItem, titleQueryItem]

        let urlWithFunction = sut?.createURLWithComponents(scheme: "https", host: "netflixroulette.net", path: "/api/api.php", queryItems: ["title": "The Boondocks", "year": "2005"])

        let urlComponentsQuery = Set((urlComponents.query?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.components(separatedBy: "&"))!)
        let urlWithFunctionQuery = Set((urlWithFunction?.query?.components(separatedBy: "&"))!)

        XCTAssertEqual(urlComponents.url, urlWithFunction)
        XCTAssertTrue(urlComponents.queryItems?.count == 2)
        XCTAssertEqual(urlComponentsQuery, urlWithFunctionQuery)
    }

    func test_MovieAPI_GetMoviesRequest_IsSuccessful() {
        // Arrange
        // Setup network stubs
        let url = sut?.createURLWithComponents(scheme: "https", host: "netflixroulette.net", path: "/api/api.php", queryItems: ["title": "Attack on Titan"])

        let stubbedJSON: [[String: Any]] = [[
        "unit": 883,
        "show_id": 70299043,
        "show_title": "Attack on Titan",
        "release_year": "2013",
        "rating": "4.6",
        "category": "Anime",
        "show_cast": "Yuki Kaji, Yui Ishikawa, Marina Inoue",
        "director": "Random Director",
        "summary": "Placeholder for summary.",
        "poster": "https://netflixroulette.net/api/posters/70299043.jpg",
        "mediatype": 1,
        "runtime": "24 min"],
        ["unit": 884,
        "show_id": 70299044,
        "show_title": "Attack on Titans",
        "release_year": "2015",
        "rating": "4.8",
        "category": "Anime",
        "show_cast": "Yuki Kaji, Yui Ishikawa, Marina Inoue",
        "director": "Random Director 2",
        "summary": "Placeholder for summary.",
        "poster": "https://netflixroulette.net/api/posters/70299043.jpg",
        "mediatype": 1,
        "runtime": "24 min"]]

        let stubbedJSONMovieOneTitle = stubbedJSON[0]["show_title"] as? String

        stub(condition: isScheme((url?.scheme)!) && isHost((url?.host)!) && isPath((url?.path)!)) { (request) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(jsonObject: stubbedJSON, statusCode: 200, headers: .none)
        }

        let completionHandlerExpectation = expectation(description: "calls the callback with a Movie object")

        // Act
        sut?.getMovieRequest(scheme: "https", host: "netflixroulette.net", path: "/api/api.php", queryItems: ["title": "Attack on Titan"]) { (movie, error) in
            
            guard let movie = movie else { return }
            
            if movie.count > 0 {
                self.movieResultsViewController?.movieManager?.movieResults = movie
            }

            // Assert
            XCTAssertNil(error)
            XCTAssertEqual(movie[0].title, stubbedJSONMovieOneTitle)
            XCTAssertNotNil(self.movieResultsViewController?.movieManager?.movieResults)
            completionHandlerExpectation.fulfill()
        }

        waitForExpectations(timeout: 3, handler: nil)
    }

    func test_MovieAPI_GetMoviesRequest_IsUnsuccessful() {
        // Arrange
        // Setup network stubs
        let url = sut?.createURLWithComponents(scheme: "https", host: "netflixroulette.net", path: "/api/api.php", queryItems: ["title": "Attack on Titan"])

        let expectedError = NSError(domain: "test", code: 42, userInfo: nil)

        stub(condition: isScheme((url?.scheme)!) && isHost((url?.host)!) && isPath((url?.path)!)) { (request) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(error: expectedError)
        }

        let completionHandlerExpectation = expectation(description: "calls the callback with an error")

        // Act
        sut?.getMovieRequest(scheme: "https", host: "netflixroulette.net", path: "/api/api.php", queryItems: ["title": "Attack on Titan"]) { (movie, error) in

            // Assert
            XCTAssertNil(movie)
            XCTAssertEqual(error as NSError?, expectedError)
            completionHandlerExpectation.fulfill()
        }
        waitForExpectations(timeout: 3, handler: nil)
    }
}
