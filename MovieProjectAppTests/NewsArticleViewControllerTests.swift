//
//  NewsArticleViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class NewsArticleViewControllerTests: XCTestCase {

    var sut: NewsArticleViewController?
    var dataProvider: NewsArticleDataProvider?
    var tableView: UITableView?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: newsArticleViewControllerIdentifier) as? NewsArticleViewController
        dataProvider = sut?.dataProvider as? NewsArticleDataProvider
        dataProvider?.newsArticleManager = NewsArticleManager()
        _ = sut?.view
        tableView = sut?.tableView
    }

    override func tearDown() {
        sut = nil
        dataProvider = nil
        tableView = nil
        super.tearDown()
    }

    func test_TableViewIsNotNilAfterViewDidLoad() {
        XCTAssertNotNil(sut?.tableView)
    }
    
    func test_RefreshControlIsNotNilAfterViewDidLoad() {
        XCTAssertNotNil(sut?.refreshControl)
    }

    func test_ActivityIndicatorViewIsNotNilAfterViewDidLoad() {
        XCTAssertNotNil(sut?.activityIndicatorView)
    }

    func test_ActivityIndicatorViewStartsAnimatingAfterViewDidLoad() {
        XCTAssertTrue((sut?.activityIndicatorView?.isAnimating)!)
    }

    func test_LoadingView_SetsTableViewDataSource() {
        XCTAssertTrue(sut?.tableView?.dataSource is NewsArticleDataProvider)
    }

    func test_LoadingView_SetsTableViewDelegate() {
        XCTAssertTrue(sut?.tableView?.delegate is NewsArticleDataProvider)
    }

    func test_LoadingView_SetsDataSourceAndDelegateToSameObject() {
        XCTAssertEqual(sut?.tableView?.dataSource as? NewsArticleDataProvider, sut?.tableView?.delegate as? NewsArticleDataProvider)
    }

    func test_ViewDidLoad_SetsNewsArticleManagerToManagerOfDataProvider() {
        XCTAssertTrue(sut?.newsArticleManager === sut?.dataProvider?.newsArticleManager)
    }

    func test_NewsArticleSelected_PushesNewsArticleDetailViewController() {
        let mockNavigationController = MockNavigationController(rootViewController: sut!)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        _ = sut?.view // addObserver called in viewDidLoad
        sut?.newsArticleManager?.newsArticles.append(NewsArticle(title: "", description: "", url: "", imageUrl: ""))

        NotificationCenter.default.post(name: NSNotification.Name(newsArticleSelectedNotification), object: nil, userInfo: [indexInfo: 0])
        guard let newsArticleDetailViewController = mockNavigationController.pushedViewController as? NewsArticleDetailViewController else { XCTFail(); return }
        guard let newsArticleManager = newsArticleDetailViewController.newsArticleInfo?.0 else { XCTFail(); return }
        guard let index = newsArticleDetailViewController.newsArticleInfo?.1 else { XCTFail(); return }

        XCTAssertTrue(newsArticleManager === sut?.newsArticleManager)
        XCTAssertEqual(index, 0)
    }
}

extension NewsArticleViewControllerTests {
    class MockNavigationController: UINavigationController {
        var pushedViewController: UIViewController?

        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            pushedViewController = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
