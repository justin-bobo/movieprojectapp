//
//  FindMoviesViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/25/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
import SwiftyJSON
@testable import MovieProjectApp

class FindMoviesViewControllerTests: XCTestCase {

    var sut: FindMoviesViewController?
    var movieResultsViewController: MovieResultsViewController?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: findMoviesViewControllerIdentifier) as? FindMoviesViewController
        _ = sut?.view
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_TextFieldsAndButtonAreNotNilAfterViewDidLoad() {
        XCTAssertNotNil(sut?.actorTextField)
        XCTAssertNotNil(sut?.directorTextField)
        XCTAssertNotNil(sut?.searchButton)
    }

    func test_LoadingView_SetsTextFieldDelegate() {
        XCTAssertTrue(sut?.actorTextField?.delegate is FindMoviesDataProvider)
        XCTAssertTrue(sut?.directorTextField?.delegate is FindMoviesDataProvider)
    }

    func test_ViewDidLoad_SetsFindMoviesManagerToManagerOfDataProvider() {
        XCTAssertTrue(sut?.movieManager === sut?.dataProvider?.movieManager)
    }

    func test_SearchButtonSelected_WithParametersEntered_ProperlySetsQueryItems() {
        createMockNavController(sut: sut!)

        sut?.actorTextField?.text = "Donald Trump"
        sut?.directorTextField?.text = "Quentin Tarantino"
        
        guard let searchButton = sut?.searchButton else { XCTFail(); return }

        sut?.search(searchButton)

        XCTAssertEqual((sut?.queryParameters)!, ["actor": "Donald Trump", "director": "Quentin Tarantino"])
    }

    func test_NotificationPosted_WithParametersEntered_ProperlySetsQueryItems() {
        createMockNavController(sut: sut!)

        sut?.actorTextField?.text = "Donald Trump"
        sut?.directorTextField?.text = "Quentin Tarantino"

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: movieSearchNotification), object: self, userInfo: nil)

        XCTAssertEqual((sut?.queryParameters)!, ["actor": "Donald Trump", "director": "Quentin Tarantino"])
    }
    
    func test_SearchButtonSelected_WithNoParametersEntered_DisplaysAlertView() {
        createMockNavController(sut: sut!)

        sut?.actorTextField?.text = ""
        sut?.directorTextField?.text = ""

        guard let searchButton = sut?.searchButton else { XCTFail(); return }

        sut?.search(searchButton)

        XCTAssertNotNil(sut?.needParametersAlert)
    }

    func test_NotificationPosted_WithNoParametersEntered_DisplaysAlertView() {
        createMockNavController(sut: sut!)

        sut?.actorTextField?.text = ""
        sut?.directorTextField?.text = ""

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: movieSearchNotification), object: self, userInfo: nil)

        XCTAssertNotNil(sut?.needParametersAlert)
    }

    func createMockNavController(sut: UIViewController) {
        let mockNavigationController = MockNavigationController(rootViewController: sut)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
    }
}

extension FindMoviesViewControllerTests {
    class MockNavigationController: UINavigationController {

        var pushedViewController: UIViewController?

        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            pushedViewController = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
