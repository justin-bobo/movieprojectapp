//
//  FindMoviesDataProviderTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/25/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class FindMoviesDataProviderTests: XCTestCase {

    var sut: FindMoviesDataProvider?
    var controller: FindMoviesViewController?
    var titleTextField: UITextField?
    var actorTextField: UITextField?
    var directorTextField: UITextField?

    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: findMoviesViewControllerIdentifier) as? FindMoviesViewController
        sut = controller?.dataProvider as? FindMoviesDataProvider
        sut?.movieManager = MovieManager()
        _ = controller?.view

        actorTextField = controller?.actorTextField
        actorTextField?.delegate = sut

        directorTextField = controller?.directorTextField
        directorTextField?.delegate = sut
    }

    override func tearDown() {
        sut = nil
        controller = nil
        titleTextField = nil
        actorTextField = nil
        directorTextField = nil
        super.tearDown()
    }
}
