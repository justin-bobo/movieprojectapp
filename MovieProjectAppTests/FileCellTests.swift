//
//  FileCellTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 4/6/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class FileCellTests: XCTestCase {
    
    var controller: FileViewController?
    var tableView: UITableView?
    var cell: FileCell?
    
    override func setUp() {
        super.setUp()
        controller = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: fileViewControllerIdentifier) as? FileViewController
        _ = controller?.view
        tableView = controller?.tableView
        let dataSource = FakeDataSource()
        tableView?.dataSource = dataSource
        tableView?.delegate = dataSource
        cell = tableView?.dequeueReusableCell(withIdentifier: fileCellIdentifier, for: IndexPath(row: 0, section: 0)) as? FileCell
    }
    
    override func tearDown() {
        controller = nil
        tableView = nil
        cell = nil
        super.tearDown()
    }
    
    func test_HasLabels() {
        XCTAssertNotNil(cell?.titleLabel)
    }
    
    func test_ConfigCell_SetsLabelsAndImage() {
        let fakeData = "{\"fake\": \"data\"}".data(using: .utf8)
        let file = File(id: "", title: "Sample title", data: fakeData!, url: URL(string: "https://www.apple.com/")!)
        cell?.configCell(with: file)
        XCTAssertEqual(cell?.titleLabel!.text, file.title)
    }
}

extension FileCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return UITableViewCell()
        }
    }
}
