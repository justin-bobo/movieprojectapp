//
//  NewsArticleAPIClientTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/24/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
import SwiftyJSON
@testable import MovieProjectApp

class NewsArticleAPIClientTests: XCTestCase {

    var sut: APIClient?

    override func setUp() {
        super.setUp()
        sut = APIClient()
    }

    override func tearDown() {
        sut = nil
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func test_CreateURLFunctionWithHardcodedURL_ReturnsCorrectURL() {
        let hardcodedURL = URL(string: "https://newsapi.org/v1/articles?source=entertainment-weekly&sortBy=top&apiKey=a7d7b2c88f77419081cabf6c5e612784")
        let urlWithFunction = sut?.createURLWithComponents(scheme: "https", host: "newsapi.org", path: "/v1/articles", queryItems: ["source": "entertainment-weekly", "sortBy": "top", "apiKey": "a7d7b2c88f77419081cabf6c5e612784"])

        let hardcodedURLQuery = Set((hardcodedURL?.query?.components(separatedBy: "&"))!)
        let urlWithFunctionQuery = Set((urlWithFunction?.query?.components(separatedBy: "&"))!)

        XCTAssertEqual(hardcodedURLQuery, urlWithFunctionQuery)
    }

    func test_CreateURLFunctionWithURLComponents_ReturnsCorrectURL() {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "newsapi.org"
        urlComponents.path = "/v1/articles"
        let sortByQueryItem = URLQueryItem(name: "sortBy", value: "top")
        let apiKeyQueryItem = URLQueryItem(name: "apiKey", value: "a7d7b2c88f77419081cabf6c5e612784")
        let sourceQueryItem = URLQueryItem(name: "source", value: "entertainment-weekly")
        urlComponents.queryItems = [sortByQueryItem, apiKeyQueryItem, sourceQueryItem]

        let urlWithFunction = sut?.createURLWithComponents(scheme: "https", host: "newsapi.org", path: "/v1/articles", queryItems: ["source": "entertainment-weekly", "sortBy": "top", "apiKey": "a7d7b2c88f77419081cabf6c5e612784"])

        let urlComponentsQuery = Set((urlComponents.query?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.components(separatedBy: "&"))!)
        let urlWithFunctionQuery = Set((urlWithFunction?.query?.components(separatedBy: "&"))!)

        XCTAssertEqual(urlComponents.url, urlWithFunction)
        XCTAssertTrue(urlComponents.queryItems?.count == 3)
        XCTAssertEqual(urlComponentsQuery, urlWithFunctionQuery)
    }

    func test_NewsArticleAPI_GetNewsArticleRequest_IsSuccessful() {
        // Arrange
        // Setup network stubs
        let url = sut?.createURLWithComponents(scheme: "https", host: "newsapi.org", path: "/v1/articles", queryItems: ["source": "entertainment-weekly", "sortBy": "top", "apiKey": "a7d7b2c88f77419081cabf6c5e612784"])

        let stubbedJSON: [String: Any] = [
            "status": "ok",
            "source": "entertainment-weekly",
            "sortBy": "top",
            "articles": [["author": "Clark Collis",
                         "title": "Random Title",
                         "description": "Random Description",
                         "url": "https://ew.com/movies/2017/03/24/guardians-of-the-galaxy-vol-2-teaser-bowie/",
                         "urlToImage": "https://i0.wp.com/ewedit.files.wordpress.com/2017/03/c7ia3qpvmaad5mp.jpg?crop=0px%2C0px%2C1800px%2C1350px&resize=660%2C495&ssl=1",
                         "publishedAt": "2017-03-24T18:53:46Z"],
                         ["author": "Oliver Gettell",
                          "title": "Random Title 2",
                          "description": "Random Description 2",
                          "url": "https://ew.com/movies/2017/03/24/chris-pratt-foosball-childrens-hospital/",
                          "urlToImage": "https://i0.wp.com/ewedit.files.wordpress.com/2017/03/gettyimages-629991818.jpg?crop=150px%2C0px%2C2400px%2C1800px&resize=660%2C495&ssl=1",
                          "publishedAt": "2017-03-24T21:46:43Z"]]]

        let stubbedJSONArticle = stubbedJSON["articles"] as? [[String: String]]
        let stubbedJSONTitle = stubbedJSONArticle?[0]["title"]

        stub(condition: isScheme((url?.scheme)!) && isHost((url?.host)!) && isPath((url?.path)!)) { (_) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(jsonObject: stubbedJSON, statusCode: 200, headers: .none)
        }

        let completionHandlerExpectation = expectation(description: "calls the callback with a NewsArticle object")

        // Act
        sut?.getNewsArticleRequest(scheme: "https", host: "newsapi.org", path: "/v1/articles", queryItems: ["source": "entertainment-weekly", "sortBy": "top", "apiKey": "a7d7b2c88f77419081cabf6c5e612784"]) { (newsArticles, error) in

            // Assert
            XCTAssertNil(error)
            XCTAssertEqual(newsArticles?[0].title, stubbedJSONTitle)
            completionHandlerExpectation.fulfill()
        }

        waitForExpectations(timeout: 3, handler: nil)
    }

    func test_NewsArticleAPI_GetNewsArticleRequest_IsUnsuccessful() {
        // Arrange
        // Setup network stubs
        let url = sut?.createURLWithComponents(scheme: "https", host: "newsapi.org", path: "/v1/articles", queryItems: ["source": "entertainment-weekly", "sortBy": "top", "apiKey": "a7d7b2c88f77419081cabf6c5e612784"])

        let expectedError = NSError(domain: "test", code: 42, userInfo: nil)

        stub(condition: isScheme((url?.scheme)!) && isHost((url?.host)!) && isPath((url?.path)!)) { (request) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(error: expectedError)
        }

        let completionHandlerExpectation = expectation(description: "calls the callback with an error")

        // Act
        sut?.getNewsArticleRequest(scheme: "https", host: "newsapi.org", path: "/v1/articles", queryItems: ["source": "entertainment-weekly", "sortBy": "top", "apiKey": "a7d7b2c88f77419081cabf6c5e612784"]) { (newsArticles, error) in

            // Assert
            XCTAssertNil(newsArticles)
            XCTAssertEqual(error as NSError?, expectedError)
            completionHandlerExpectation.fulfill()
        }

        waitForExpectations(timeout: 3, handler: nil)
    }
}
