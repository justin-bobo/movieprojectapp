//
//  MovieTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/15/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class MovieTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_Movie_IsNotNil() {
        let movie = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        XCTAssertNotNil(movie)
    }

    func test_MovieAttributes_AreEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        moviesEqual(movie2: movie2)
    }

    func test_MovieAttributes_AreNotEqual() {
        let movie2 = Movie(id: 555550, title: "Jingle All the Wayyyy", releaseYear: "", rating: "", category: "Family", cast: "Random dude", director: "Who cares", summary: "Sometext", runtime: "90 min", poster: "imageURL placeholder text")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieIDs_AreNotEqual() {
        let movie2 = Movie(id: 555550, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieTitles_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Wayy", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieReleaseYears_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "1995", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieRatings_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "3.2", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieCategories_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Family", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieCasts_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Random Dude", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieDirectors_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Random Person", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieSummaries_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Random text", runtime: "77 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MovieRuntimes_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "78 min", poster: "imageURL placeholder")
        moviesNotEqual(movie2: movie2)
    }

    func test_MoviePosters_AreNotEqual() {
        let movie2 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder text")
        moviesNotEqual(movie2: movie2)
    }

    func moviesEqual(movie2: Movie) {
        let movie1 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        XCTAssertEqual(movie1, movie2)
    }

    func moviesNotEqual(movie2: Movie) {
        let movie1 = Movie(id: 555555, title: "Jingle All the Way", releaseYear: "", rating: "", category: "Comedy", cast: "Arnold Schwarzenegger", director: "Jackie Chan", summary: "Placeholder text", runtime: "77 min", poster: "imageURL placeholder")
        XCTAssertNotEqual(movie1, movie2)
    }
}
