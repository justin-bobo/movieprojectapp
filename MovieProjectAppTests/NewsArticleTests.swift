//
//  NewsArticleTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/15/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class NewsArticleTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_NewsArticle_IsNotNil() {
        let newsArticle = NewsArticle(title: "", description: "", url: "", imageUrl: "")
        XCTAssertNotNil(newsArticle)
    }

    func test_NewsArticles_AreEqual() {
        let newsArticle2 = NewsArticle(title: "Trump says something stupid", description: "Trump at it again", url: "url placeholder", imageUrl: "imageUrl placeholder")
        newsArticles_AreEqual(newsArticle2: newsArticle2)
    }

    func test_NewsArticles_AreNotEqual() {
        let newsArticle2 = NewsArticle(title: "Trump says something dumb", description: "Trump at it again lol", url: "url placeholder 2", imageUrl: "imageUrl placeholder 2")
        newsArticles_AreNotEqual(newsArticle2: newsArticle2)
    }

    func test_NewsArticleTitles_AreNotEqual() {
        let newsArticle2 = NewsArticle(title: "Trump says something dumb", description: "Trump at it again", url: "url placeholder", imageUrl: "imageUrl placeholder")
        newsArticles_AreNotEqual(newsArticle2: newsArticle2)
    }

    func test_NewsArticleDescriptions_AreNotEqual() {
        let newsArticle2 = NewsArticle(title: "Trump says something stupid", description: "Trump at it again lol", url: "url placeholder", imageUrl: "imageUrl placeholder")
        newsArticles_AreNotEqual(newsArticle2: newsArticle2)
    }

    func test_NewsArticleUrls_AreNotEqual() {
        let newsArticle2 = NewsArticle(title: "Trump says something stupid", description: "Trump at it again", url: "url placeholder 2", imageUrl: "imageUrl placeholder")
        newsArticles_AreNotEqual(newsArticle2: newsArticle2)
    }

    func test_NewsArticleImageUrls_AreNotEqual() {
        let newsArticle2 = NewsArticle(title: "Trump says something stupid", description: "Trump at it again", url: "url placeholder", imageUrl: "imageUrl placeholder 2")
        newsArticles_AreNotEqual(newsArticle2: newsArticle2)
    }

    func newsArticles_AreEqual(newsArticle2: NewsArticle) {
        let newsArticle1 = NewsArticle(title: "Trump says something stupid", description: "Trump at it again", url: "url placeholder", imageUrl: "imageUrl placeholder")
        XCTAssertEqual(newsArticle1, newsArticle2)
    }

    func newsArticles_AreNotEqual(newsArticle2: NewsArticle) {
        let newsArticle1 = NewsArticle(title: "Trump says something stupid", description: "Trump at it again", url: "url placeholder", imageUrl: "imageUrl placeholder")
        XCTAssertNotEqual(newsArticle1, newsArticle2)
    }
}
