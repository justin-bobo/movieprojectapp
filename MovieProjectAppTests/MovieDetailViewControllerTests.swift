//
//  MovieDetailViewControllerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/19/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class MovieDetailViewControllerTests: XCTestCase {

    var sut: MovieDetailViewController?
    var savedMovieViewController: SavedMovieViewController?
    var movieResultsViewController: MovieResultsViewController?
    var mockNavigationController: MockNavigationController?

    override func setUp() {
        super.setUp()
        sut = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: movieDetailViewControllerIdentifier) as? MovieDetailViewController
        _ = sut?.view
        savedMovieViewController = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: savedMovieViewControllerIdentifier) as? SavedMovieViewController
        movieResultsViewController = UIStoryboard(name: mainStoryboard, bundle: nil).instantiateViewController(withIdentifier: movieResultsViewControllerIdentifier) as? MovieResultsViewController
    }

    override func tearDown() {
        sut = nil
        savedMovieViewController = nil
        movieResultsViewController = nil
        mockNavigationController = nil
        MovieManager.sharedInstance.savedMovies.removeAll()
        super.tearDown()
    }

    func test_HasLabelsAndPosterImage() {
        XCTAssertNotNil(sut?.titleLabel)
        XCTAssertNotNil(sut?.releaseYearLabel)
        XCTAssertNotNil(sut?.ratingLabel)
        XCTAssertNotNil(sut?.categoryLabel)
        XCTAssertNotNil(sut?.castLabel)
        XCTAssertNotNil(sut?.directorLabel)
        XCTAssertNotNil(sut?.summaryLabel)
        XCTAssertNotNil(sut?.runtimeLabel)
        XCTAssertNotNil(sut?.posterImage)
    }

    func test_SettingSavedMovieInfo_SetsTextsToLabelsAndPosterToImageView() {
        let movie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "2000", rating: "3.5", category: "Comedy", cast: "Donald Trump", director: "Arnold S", summary: "Donald Trump is an idiot", runtime: "90 min", poster: "https://www.demilked.com/magazine/wp-content/uploads/2011/10/apple-logo-steve-jobs-silhouette.jpg")
        sut?.movie = movie

        callViewDidAppear(sut: sut!)

        XCTAssertEqual(sut?.titleLabel?.text, "Trump is Dumb")
        XCTAssertEqual(sut?.releaseYearLabel?.text, "2000")
        XCTAssertEqual(sut?.ratingLabel?.text, "3.5")
        XCTAssertEqual(sut?.categoryLabel?.text, "Comedy")
        XCTAssertEqual(sut?.castLabel?.text, "Donald Trump")
        XCTAssertEqual(sut?.directorLabel?.text, "Arnold S")
        XCTAssertEqual(sut?.summaryLabel?.text, "Donald Trump is an idiot")
        XCTAssertEqual(sut?.runtimeLabel?.text, "90 min")
        XCTAssertNotNil(sut?.posterImage?.image)
    }

    func test_PosterImageCallsDropShadow() {
        let movie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "2000", rating: "3.5", category: "Comedy", cast: "Donald Trump", director: "Arnold S", summary: "Donald Trump is an idiot", runtime: "90 min", poster: "https://www.demilked.com/magazine/wp-content/uploads/2011/10/apple-logo-steve-jobs-silhouette.jpg")
        sut?.movie = movie
        
        callViewDidAppear(sut: sut!)
        
        XCTAssertEqual(sut?.posterImage?.layer.shadowColor, UIColor.black.cgColor)
        XCTAssertEqual(sut?.posterImage?.layer.shadowOpacity, 0.5)
        XCTAssertEqual(sut?.posterImage?.layer.shadowOffset, CGSize(width: -1, height: 1))
        XCTAssertEqual(sut?.posterImage?.layer.shadowRadius, 1)
    }
    
    func test_SaveButton_AddsMovieToSavedMoviesArray() {
        sut?.movie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "2000", rating: "3.5", category: "Comedy", cast: "Donald Trump", director: "Arnold S", summary: "Donald Trump is an idiot", runtime: "90 min", poster: "https://www.demilked.com/magazine/wp-content/uploads/2011/10/apple-logo-steve-jobs-silhouette.jpg")
        let movieManager = MovieManager.sharedInstance
        sut?.movieInfo = (movieManager, 0)
        
        callViewDidAppear(sut: sut!)
        
        sut?.saveMovie()
        XCTAssertTrue(sut?.movieInfo?.0.savedMovieCount == 1)
    }
    
    func test_TrashButton_RemovesMovieFromSavedMoviesArray() {
        sut?.movie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "2000", rating: "3.5", category: "Comedy", cast: "Donald Trump", director: "Arnold S", summary: "Donald Trump is an idiot", runtime: "90 min", poster: "https://www.demilked.com/magazine/wp-content/uploads/2011/10/apple-logo-steve-jobs-silhouette.jpg")
        let movieManager = MovieManager.sharedInstance
        sut?.movieInfo = (movieManager, 0)
        
        callViewDidAppear(sut: sut!)
        
        sut?.saveMovie()
        sut?.removeMovie()
        XCTAssertTrue(sut?.movieInfo?.0.savedMovieCount == 0)
    }
    
    func test_Deinit_SetsMovieResultsToNil() {
        sut?.movie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "2000", rating: "3.5", category: "Comedy", cast: "Donald Trump", director: "Arnold S", summary: "Donald Trump is an idiot", runtime: "90 min", poster: "https://www.demilked.com/magazine/wp-content/uploads/2011/10/apple-logo-steve-jobs-silhouette.jpg")
        sut?.movieInfo?.0.movieResults = [(sut?.movie)!]
        sut = nil
        XCTAssertNil(sut?.movieInfo?.0.movieResults)
    }
    
    func test_ViewWillAppear_SetsButtonToTrashAndAlertToMovieRemoved() {
        let movie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "2000", rating: "3.5", category: "Comedy", cast: "Donald Trump", director: "Arnold S", summary: "Donald Trump is an idiot", runtime: "90 min", poster: "https://www.demilked.com/magazine/wp-content/uploads/2011/10/apple-logo-steve-jobs-silhouette.jpg")
        sut?.movie = movie
        let movieManager = MovieManager.sharedInstance
        sut?.movieInfo = (movieManager, 0)
        sut?.movieInfo?.0.savedMovies = [movie]
        
        callViewDidAppear(sut: sut!)
        
        XCTAssertEqual(sut?.saveButtonType?.action, #selector(sut?.removeMovie))
        XCTAssertTrue(sut?.alert?.title == "Movie Removed")
    }
    
    func test_ViewWillAppear_SetsButtonToSaveAndAlertToMovieSaved() {
        let movie = Movie(id: 0, title: "Trump is Dumb", releaseYear: "2000", rating: "3.5", category: "Comedy", cast: "Donald Trump", director: "Arnold S", summary: "Donald Trump is an idiot", runtime: "90 min", poster: "https://www.demilked.com/magazine/wp-content/uploads/2011/10/apple-logo-steve-jobs-silhouette.jpg")
        sut?.movie = movie
        let movieManager = MovieManager.sharedInstance
        sut?.movieInfo = (movieManager, 0)
        sut?.movieInfo?.0.savedMovies = [movie]
        sut?.movieInfo?.0.savedMovies.removeAll()
        
        callViewDidAppear(sut: sut!)
        
        XCTAssertEqual(sut?.saveButtonType?.action, #selector(sut?.saveMovie))
        XCTAssertTrue(sut?.alert?.title == "Movie Saved")
    }
    
    func test_RemoveMovie_PresentsAlert_DisplaysTitle() {
        mockNavigationController = MockNavigationController(rootViewController: savedMovieViewController!)
        UIApplication.shared.keyWindow?.rootViewController = savedMovieViewController!

        let movie = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")

        savedMovieViewController?.movieManager?.savedMovies = [movie]
        savedMovieViewController?.viewDidAppear(true)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: savedMovieSelectedNotification), object: savedMovieViewController, userInfo: [indexInfo: 0])

        guard let sut = mockNavigationController?.pushedViewController as? MovieDetailViewController else { XCTFail(); return }

        callViewDidAppear(sut: sut)

        // need to set the sut as the rootViewController
        UIApplication.shared.keyWindow?.rootViewController = sut

        sut.removeMovie()

        XCTAssertNotNil(sut.alert)
        XCTAssertNotNil(sut.alertAction)
        XCTAssertTrue(sut.presentedViewController is UIAlertController)
        XCTAssertEqual(sut.presentedViewController?.title, "Movie Removed")
        XCTAssertEqual(savedMovieViewController?.movieManager?.savedMovieCount, 0)
    }

    func test_SaveMovie_PresentsAlertAndDisplaysTitle_AndTogglesButton() {
        mockNavigationController = MockNavigationController(rootViewController: movieResultsViewController!)
        UIApplication.shared.keyWindow?.rootViewController = movieResultsViewController!

        let movie = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")

        movieResultsViewController?.movieManager = MovieManager.sharedInstance
        movieResultsViewController?.movieManager?.movieResults = [movie]
        movieResultsViewController?.viewDidAppear(true)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: movieResultSelectedNotification), object: movieResultsViewController, userInfo: [indexInfo: 0])

        guard let sut = mockNavigationController?.pushedViewController as? MovieDetailViewController else { XCTFail(); return }

        callViewDidAppear(sut: sut)

        // need to set the sut as the rootViewController
        UIApplication.shared.keyWindow?.rootViewController = sut
        
        sut.saveMovie()

        XCTAssertNotNil(sut.alert)
        XCTAssertNotNil(sut.alertAction)
        XCTAssertTrue(sut.presentedViewController is UIAlertController)
        XCTAssertEqual(sut.presentedViewController?.title, "Movie Saved")
        XCTAssertEqual(movieResultsViewController?.movieManager?.savedMovieCount, 1)
        XCTAssertEqual(sut.saveButtonType?.action, #selector(sut.removeMovie))
    }
    
    func test_SavingAndRemovingMovie_PresentsCorrectAlertAndTitle_AndTogglesButton() {
        mockNavigationController = MockNavigationController(rootViewController: movieResultsViewController!)
        UIApplication.shared.keyWindow?.rootViewController = movieResultsViewController!
        
        let movie = Movie(id: 0, title: "Trump", releaseYear: "", rating: "", category: "", cast: "", director: "", summary: "", runtime: "", poster: "")
        
        movieResultsViewController?.movieManager = MovieManager.sharedInstance
        movieResultsViewController?.movieManager?.movieResults = [movie]
        // need to set savedMovies here - using saveMovies function creates confusion for test
        movieResultsViewController?.movieManager?.savedMovies = [movie]
        movieResultsViewController?.viewDidAppear(true)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: movieResultSelectedNotification), object: movieResultsViewController, userInfo: [indexInfo: 0])
        
        guard let sut = mockNavigationController?.pushedViewController as? MovieDetailViewController else { XCTFail(); return }
        
        callViewDidAppear(sut: sut)
        
        // need to set the sut as the rootViewController
        UIApplication.shared.keyWindow?.rootViewController = sut
        
        sut.removeMovie()
        
        XCTAssertNotNil(sut.alert)
        XCTAssertNotNil(sut.alertAction)
        XCTAssertTrue(sut.presentedViewController is UIAlertController)
        XCTAssertFalse((sut.movieInfo?.0.savedMovies.contains(movie))!)
        XCTAssertEqual(sut.presentedViewController?.title, "Movie Removed")
        XCTAssertEqual(movieResultsViewController?.movieManager?.savedMovieCount, 0)
        XCTAssertEqual(sut.saveButtonType?.action, #selector(sut.saveMovie))
    }
    
    func callViewDidAppear(sut: UIViewController) {
        // triggers call of viewWillAppear
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
    }
}

extension MovieDetailViewControllerTests {
    class MockNavigationController: UINavigationController {
        var pushedViewController: UIViewController?
        
        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            pushedViewController = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
