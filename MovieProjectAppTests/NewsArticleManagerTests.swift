//
//  NewsArticleManagerTests.swift
//  MovieProjectApp
//
//  Created by Justin Bobo on 3/16/17.
//  Copyright © 2017 Collar. All rights reserved.
//

import XCTest
@testable import MovieProjectApp

class NewsArticleManagerTests: XCTestCase {
    
    var sut: NewsArticleManager?
    
    override func setUp() {
        super.setUp()
        sut = NewsArticleManager()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_NewsArticleCount_IsInitiallyZero() {
        let newsArticleCount = sut?.newsArticleCount
        XCTAssertEqual(newsArticleCount, 0)
    }

    func test_NewsArticleCount_EqualsOne_WhenNewsArticleAdded() {
        let newsArticle = NewsArticle(title: "", description: "", url: "", imageUrl: "")
        sut?.add(newsArticle: newsArticle)
        let newsArticleCount = sut?.newsArticleCount
        XCTAssertEqual(newsArticleCount, 1)
    }

    func test_NewsArticleCount_EqualsZero_WhenNewsArticleAddedAndDeleted() {
        let newsArticle = NewsArticle(title: "", description: "", url: "", imageUrl: "")
        sut?.add(newsArticle: newsArticle)
        sut?.remove(at: 0)
        let newsArticleCount = sut?.newsArticleCount
        XCTAssertEqual(newsArticleCount, 0)
    }

    func test_NewsArticleAt_AfterAddingNewsArticle_ReturnsThatNewsArticle() {
        let newsArticle1 = NewsArticle(title: "", description: "", url: "", imageUrl: "")
        sut?.add(newsArticle: newsArticle1)
        let returnedNewsArticle = sut?.returnNewsArticle(at: 0)
        XCTAssertEqual(newsArticle1, returnedNewsArticle)
    }

    func test_NewsArticleAt_AfterAddingTwoNewsArticles_ReturnsSecondNewsArticle() {
        let newsArticle1 = NewsArticle(title: "", description: "", url: "", imageUrl: "")
        let newsArticle2 = NewsArticle(title: "hey", description: "", url: "", imageUrl: "")
        sut?.add(newsArticle: newsArticle1)
        sut?.add(newsArticle: newsArticle2)
        let returnedNewsArticle = sut?.returnNewsArticle(at: 1)
        XCTAssertEqual(newsArticle2, returnedNewsArticle)
    }

    func test_AttemptingToAddSameNewsArticleTwice_OnlyAddsItOnce() {
        let newsArticle1 = NewsArticle(title: "", description: "", url: "", imageUrl: "")
        let newsArticle2 = NewsArticle(title: "", description: "", url: "", imageUrl: "")
        sut?.add(newsArticle: newsArticle1)
        sut?.add(newsArticle: newsArticle2)
        XCTAssertEqual(sut?.newsArticleCount, 1)
    }
}
